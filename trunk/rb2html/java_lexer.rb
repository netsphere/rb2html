# -*- coding:utf-8 -*-
# rb2html
# Copyright (c) 2001, 2004, 2006 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/
#     mailto:vzw00011@nifty.ne.jp

if !$__RB2HTML_JAVA_LEXER__
$__RB2HTML_JAVA_LEXER__ = true

require 'rb2html/pattern_lexer'

module Rb2HTML
  class JavaLexer < PatternLexer
    IDENT = /[a-zA-Z_][a-zA-Z_0-9]*/

    JAVA_PATTERNS = [
      # float
      [/([0-9]+\.[0-9]*|\.[0-9]+)([eE][+-]?[0-9]+)?[fFdD]?/, :numeric], # 0. or .0
      [/[0-9]+[eE][+-]?[0-9]+[fFdD]?/, :numeric],                       # 1e1
      [/[0-9]+([eE][+-]?[0-9]+)?[fFdD]/, :numeric],                     # 1f

      # integer
      [/0[xX][0-9a-fA-F]+[lL]?/, :numeric],
      [/[0-9]+[lL]?/, :numeric],

      # ident
      [IDENT, :ident],

      # アノテーション
      [/@interface/, :keyword],
      [/@[a-zA-Z_][a-zA-Z_0-9]*/, :annotation],

      # comment
      [%r(/\*(.|\n)*?\*/), :comment], # ネストなし
      [%r(//.*), :comment],          # 行末まで

      # operator
      [%r(\+=|-=|\*=|/=|&=|\|=|\^=|%=|<<=|>>=|>>>=), :operator],
      [/==|<=|>=|!=|&&|\|\||\+\+|--/, :operator],
      [/<<|>>>/, :operator],

      # Javaでは List<LinkedList<String>> の'>>' を判別
      [/>>/, :operator, {:only_after => [:numeric, :string]}],
      [/>>/, :op_or_bracket2],

      # 演算子かジェネリクス関係か
      [/</, :template_open, 
            {:only_after => [:typename, :block_open, :block_close, :semicolon]}], 
      [/\?/, :typename, {:only_after => :template_open}],
      [/>/, :op_or_bracket],
      [%r(=|>|<|!|~|\?|:|\+|-|\*|/|&|\||\^|%), :operator],

      # single character やや手抜き。TODO: \uFFFFなど
      [/'(\\.|[^'])*'/, :string],  #'

      # string
      [/"(\\.|[^"])*"/, :string],  #"

      [/\{/, :block_open],
      [/\}/, :block_close],
      [/\;/, :semicolon],

      [/\s+/, :space],
    ]

    # assert, enumが追加になっている。
    KEYWORDS = %w(abstract assert        
        package private protected public
        synchronized
        for if while else do continue break return
        switch case  default  goto                this
        try    throw catch
        import               
        enum          transient
        finally               strictfp     volatile
        const              native        super
        null true false)

    # 後に識別子が来るときはクラス名
    KW_CLASS = %w(class extends implements interface 
                new instanceof throws final static)

    # 後に識別子が来るときはクラス名ではない
    KW_TYPENAME = %w(boolean byte short int long char float double void)

    def ident_or_typename r
      if KEYWORDS.include?(r)
        flush_token Token.new(:keyword, r)
      elsif KW_CLASS.include?(r)
        flush_token Token.new(:kw_class, r)
      elsif KW_TYPENAME.include?(r)
        flush_token Token.new(:kw_typename, r)
      else
        if @last_kind && [:kw_class, :template_open].include?(@last_kind)
          flush_token Token.new(:typename, r)
        elsif @last_kind && [:kw_typename, :typename].include?(@last_kind)
          flush_token Token.new(:ident, r)
        elsif @last_kind && [:block_open, :block_close].include?(@last_kind)
          # 先読みしないと判別できない
          if @scanner.check(/\s+#{IDENT}/) ||
             @scanner.check(/\s*\</)
            flush_token Token.new(:typename, r)
          else
            flush_token Token.new(:ident, r)
          end
        else
          # forの中や文頭は構文解析が必要か？
          if @scanner.check(/\</)
            flush_token Token.new(:typename, r)
          else
            flush_token Token.new(:ident, r)
          end
        end
      end
    end

    
    # @override
    def parse source_, options = {}, &block
      super(source_, options)
      
      template_depth = 0
      while !@scanner.eos?
        matched = match(JAVA_PATTERNS) {|sym, r|
          case sym
          when :ident
            ident_or_typename r
          when :template_open
            flush_token Token.new(sym, r)
            template_depth += 1
          when :op_or_bracket # '>'
            if template_depth > 0
              flush_token Token.new(:template_close, r)
              template_depth -= 1
            else
              flush_token Token.new(:operator, r)
            end
          when :op_or_bracket2 # '>>'
            if template_depth >= 2
              flush_token Token.new(:template_close, '>')
              flush_token Token.new(:template_close, '>')
              template_depth -= 2
            else
              flush_token Token.new(:operator, '>>')
              template_depth = 0 # = 1 のときは何か間違えたか？
            end
          else
            flush_token Token.new(sym, r)
          end
        }

        if !matched
          @cur.symbol = :other
          @cur.text << @scanner.getch
        end
      end
      flush_token
      return @parsed
    end

  end # of class JavaLexer
end

end

