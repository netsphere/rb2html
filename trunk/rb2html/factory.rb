# -*- coding:utf-8 -*-

# rb2html
# Copyright (c) 2001, 2004, 2006 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/
#     mailto:vzw00011@nifty.ne.jp

require 'rb2html/html_formatter'
require 'rb2html/source2html'

module Rb2HTML
  class Factory
    # rb2html-conf.rb と名前を合わせること.
    TARGETS = {
      'Ruby' => {
        :ext => ['.rb'], :conv => ['ruby_lexer.rb', 'RubyLexer']
      },
      # TODO: C++ / Objective-C と, C# を分ける
      'objc' => {
        :ext => ['.m', '.mm'],
        :conv => ['c_lexer.rb', 'CLexer']
      },        
      'C++' => {
        :ext => ['.c++', '.c', '.cpp', '.cc', '.cxx', '.h'],
        :conv => ['c_lexer.rb', 'CLexer']
      },
      'csharp' => {
        :ext => ['.cs'],
        :conv => ['c_lexer.rb', 'CLexer']
      },        
      'Java' => {
        :ext => ['.java'], :conv => ['java_lexer.rb', 'JavaLexer']
      },
      'JavaScript' => {
        :ext => ['.js'], :conv => ['javascript_lexer.rb', 'JavaScriptLexer']
      },
      'Python' => {
        :ext => ['.py'], :conv => ['python_lexer.rb', 'PythonLexer']
      },
      'Haskell' => {
        :ext => ['.hs'], :conv => ['haskell_lexer.rb', 'HaskellLexer']
      },
      'HTML/XML' => {
        :ext => ['.xhtml', '.htm', '.html', '.xml', '.rdf'],
        :conv => ['html_parse.rb', 'HTMLRawParser']
      },
      'CSS' => {
        :ext => ['.css'],
        :conv => ['css_lexer.rb', 'CSSLexer']
      },
    }

    
    def Factory.has_lang? lang
      raise TypeError if !lang.is_a?(String)
      
      TARGETS.each {|lang_name, opt|
        if lang.downcase == lang_name.downcase ||
                                opt[:ext].include?("." + lang.downcase)
          return true
        end
      }
      return false
    end

    
    # @param [String] lang 言語名 or 拡張子
    # @return [Source2Html] 生成したフォーマッタ
    def Factory.get_formatter lang
      raise TypeError if !lang.is_a?(String)
      
      TARGETS.each {|lang_name, opt|
        if lang.downcase == lang_name.downcase ||
                                opt[:ext].include?("." + lang.downcase)
          require 'rb2html/' + opt[:conv][0]
          return Source2Html.new(HtmlLayout.new(lang_name), 
                                 module_eval(opt[:conv][1]).new(lang_name),
                                 lang_name)
        end
      }
      return nil
    end
  end
end # of module Rb2HTML

