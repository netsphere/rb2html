# -*- coding:utf-8 -*-

# rb2html
# Copyright (c) 2001, 2004, 2006 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/
#     mailto:vzw00011@nifty.ne.jp

if !$__RB2HTML_C_LEXER__
$__RB2HTML_C_LEXER__ = true

require 'rb2html/pattern_lexer'

module Rb2HTML
  # C++/Objective-C
  class CLexer < PatternLexer
    # C++11
    # KEYWORDS, KW_OP, KW_TYPE がキーワード.

    # TODO: 文脈依存のキーワード
    #       final override

    # 型名 (の一部) になりうるもの
    # これプラス typename, struct, class
    KW_TYPE = %w(
      auto   bool     char    char16_t  char32_t  const   decltype  double  
      float  int  
      long   register short  signed  unsigned  void   volatile   wchar_t )

    KEYWORDS = %w(
      alignas
      alignof
      asm           do            inline                           typeid       
                                                                   typename     
                    dynamic_cast                     sizeof        union        
      break         else          mutable            static             
                                                     static_assert 
      case          enum          namespace          static_cast   using        
      catch         explicit      noexcept           struct        virtual      
                    export        nullptr
                    extern        operator           switch              
         
      class         false         private            template           
                                  protected          this                
      constexpr                                      thread_local
      const_cast    for           public             throw         while        
      continue      friend                           true                   
          
      default       goto          reinterpret_cast   try            
                    if            return             typedef       )

    # C++11: 一部のキーワードと代替表現
    # 'new' の後ろは型名
    KW_OP = %w(new delete 
      and      and_eq   bitand   bitor   compl  not 
      not_eq   or       or_eq    xor     xor_eq)

    OBJC_KW = %w(
      id )

    # KEYWORDS, KW_TYPE のなかで、地の文に出てきたら型名が続くもの.
    #     cout << alignof(int)
    #     static_cast<int>(0.0)
    #     constexpr bool TrueFunction() 
    #     std::vector<int>
    #     型宣言   enum Enum: int
    #              enum class Color {...
    #     型の修飾
    #         template<class T> void Foo() { }
    BEFORE_TYPENAME = %w(
      alignof
      static_cast  dynamic_cast  reinterpret_cast   const_cast
      constexpr    extern  static
      class  struct   typename  union  enum
      const  volatile register
      typedef
      template
      thread_local 
      virtual )

    # プリプロセッサ
    PP_KEYWORDS = %w(if ifdef ifndef elif else endif include define 
                     undef line error pragma)

    # Objective-C directives .. '@'keywords
    OBJC_DIRECTIVES = %w(
      autoreleasepool  catch         class     defs   dynamic  encode   end   
      finally
      implementation   interface     optional
      package          private       property  protected  protocol   public  
      required
      selector         synchronized  synthesize   throw  try 
      compatibility_alias )

    # Array of [pattern, symbol, option]
    S0_PATTERNS = [
      # float
      [/([0-9]+\.[0-9]*|\.[0-9]+)([eE][+-]?[0-9]+)?[fFlL]?/, :numeric], # 0. or .0
      [/[0-9]+[eE][+-]?[0-9]+[fFlL]?/, :numeric],                       # 1e1

      # integer
      # LL = [C99] long long int
      [/0[xX][0-9a-fA-F]+[uU]?(LL|ll|[lL])?/, :numeric],
      [/0[xX][0-9a-fA-F]+(LL|ll|[lL])[uU]?/, :numeric],
      [/[0-9]+[uU]?(LL|ll|[lL])?/, :numeric],
      [/[0-9]+(LL|ll|[lL])[uU]?/, :numeric],

      # character literals ... 複数文字を書いてもよい。=> type int
      [/L?'(\\.|[^'])*'/, :string],  #'
      # string
      [/L?"(\\.|[^"])*"/, :string],  #"

      # ident
      [/([a-zA-Z_][a-zA-Z_0-9]*)?(::[a-zA-Z_][a-zA-Z_0-9]*)+/, :ident],
      [/[a-zA-Z_][a-zA-Z_0-9]*/, :ident],
      [/@[a-z][a-z]*/, :annotation],  # obj-c

      # comment
      [%r(/\*(.|\n)*?\*/), :comment], # ネストなし
      [%r(//.*), :comment],          # 行末まで

      # operator
      [%r(\+=|-=|\*=|/=|&=|\|=|\^=|%=|<<=|>>=), :operator],
      [/==|<=|>=|!=|&&|\|\||\+\+|--/, :operator],
      [/<<|>>/, :operator],
      [%r(=|>|<|!|~|\?|:|\+|-|\*|/|&|\||\^|%), :operator],

      [/\r?\n/, :space],  # プリプロセッサ判定のために改行を切り出す
                          # last_meaningful_token() のために :space でなければ
                          # ならない
      [/[ \t]+/, :space],
    ]

  public
    # @override
    # @param options :typename 型名の配列
    # @param options :varname 関数 / 変数名の配列
    def parse source, options = {}
      super(source, options)

      @typename = ['FILE', 'size_t', 'intptr_t', 'addrinfo', 'sockaddr',
            # C++ std名前空間
            'unary_function', 'string', 'vector', 'pair', 'unique_ptr',
            'array', 'map', 'unary_negate', 'wstring',
            'std::string', 'std::list', 'std::vector', 'std::streambuf', 'std::exception', 'std::bad_exception',
            'is_nothrow_copy_constructible',
            'is_nothrow_move_constructible', 'is_move_constructible',
            'is_nothrow_copy_assignable',
            'is_nothrow_move_assignable',
            # Win32
            'TCHAR', 'WSADATA', 'LPTSTR', 'LPVOID', 'WCHAR', 'BYTE', 'LPBYTE',
            # gtk+
            'GtkWidget', 'GdkEvent', 'GtkBuilder', 'GtkWindow',
            'NSAutoreleasePool',
            'boost::optional', 'boost::shared_ptr',
                  ] 
      @typename |= (options[:typename] || [])

      # グローバル名前空間のもののみ.
      @varname = ['printf', 'free', 'strdup', # TODO: std名前空間のもの
                  'MPI_Init',
                  'NULL', ]   # TODO: 定数はさらに分けるか??
      @varname |= (options[:varname] || [])
      
      @state = 1
      @type_stack = []

      while !@scanner.eos?
        if @scanner.beginning_of_line?
          # preprocessing directives
          if (r = @scanner.scan(/([ \t]*)(\#[ \t]*)([a-z]+)/))
            if @scanner[1] && @scanner[1] != ""
              flush_token Token.new(:space, @scanner[1])
            end
            if PP_KEYWORDS.include?(@scanner[3])
              flush_token Token.new(:cpp, @scanner[2] + @scanner[3])
            else
              flush_token Token.new(:cpp, @scanner[2])
              flush_token Token.new(:other, @scanner[3])
            end
            next
          end
        end
        
        case @state
        when 1
          state_1
        when 2 # 型名
          state_2
        else
          raise "internal error"
        end
      end
      flush_token
      return @parsed
    end

  private

    # 地の文
    def state_1
      match(S0_PATTERNS) {|sym, r|
        case sym
        when :ident
          if KEYWORDS.include?(r)
            flush_token Token.new(:keyword, r)
            if r == 'template'
              # メソッドとして必要になることがある.
              #   ex) v1.template emplace<0>("abc")
              t = last_meaningful_token(2) # 直前の template のさらに一つ前
              @state = 2 if !(t && t.text == '.')
            else
              @state = 2 if BEFORE_TYPENAME.include?(r)
            end
          elsif KW_TYPE.include?(r) || (@lang_name == 'objc' && OBJC_KW.include?(r))
            flush_token Token.new(:kw_type, r)
            if r == 'register'
              @state = 2
            elsif ['const', 'volatile'].include?(r)
              t = last_meaningful_token
              if !(t && [:kw_type, :typename, :op_type].include?(t.symbol))
                @state = 2
              end
            end
          elsif KW_OP.include?(r)
            flush_token Token.new(:kw_op, r)
            @state = 2 if r == 'new'
          else
            if @typename.include?(r)
              flush_token Token.new(:typename, r)
            elsif @varname.include?(r)
              flush_token Token.new(:variable, r)
            else 
              t = last_meaningful_token
              if t && [:kw_type, :typename, :op_type].include?(t.symbol)
                @varname << r
                flush_token Token.new(:variable, r)
              elsif t && t.symbol == :ident
                if @lang_name != 'objc'
                  t.symbol = :typename  # 一つ前の方は型名
                  @varname << r
                end
                flush_token Token.new(:variable, r)
              else
                if @lang_name == 'objc'
                  t2 = last_meaningful_token(2)
                  if (t2 && t2.text == '[' && t.symbol == :variable) ||
                     (t && t.text == ']')
                    flush_token Token.new(:method, r)
                  else
                    flush_token Token.new(sym, r) # :ident
                  end
                else # !objc
                  flush_token Token.new(sym, r) # :ident
                end
              end
            end
          end
        when :annotation
          if OBJC_DIRECTIVES.include?(r[1..-1])
            flush_token Token.new(:keyword, r)  # '@'付き
            @state = 2 if r == '@interface' || r == '@implementation'
          else
            flush_token Token.new(:ident, r)
          end
        when :operator
          case r
          when '*', '&'
            t = last_meaningful_token
            if t && (t.symbol == :kw_type || t.symbol == :typename)
              flush_token Token.new(:op_type, r)
              return 
            end
          when '<'
            t = last_meaningful_token
            if t && t.symbol == :typename
              flush_token Token.new(:op_type, r)
              @type_stack.unshift '>'
              return 
            end
          when '>'
            if @type_stack.index('>')
              t = last_meaningful_token
              if t && [:typename, :kw_type, :op_type].include?(t.symbol)
                flush_token Token.new(:op_type, '>')
                @type_stack = @type_stack.drop( @type_stack.index('>') + 1 )
                return
              end
            end
          when ':'
            # class XXX: YYY
            t = last_meaningful_token
            @state = 2 if t && t.symbol == :typename
          when '='  # TODO: binary operator 全般にする
            t = last_meaningful_token
            if t && (t.symbol == :ident)
              t.symbol = :variable
            end
          end
          flush_token Token.new(sym, r) # :operator
        else
          flush_token Token.new(sym, r)
        end
        return
      }
      # マッチなし
      ch = @scanner.getch
      if ch == ','
        t = last_meaningful_token
        if t && [:typename, :kw_type, :op_type].include?(t.symbol)
          @state = 2
        end
      end
      @cur.symbol = :other
      @cur.text << ch
    end


    # 型名が想定される場所. unsigned int など、複数語の可能性あり。
    def state_2
      match(S0_PATTERNS) {|sym, r|
        case sym
        when :ident
          if KEYWORDS.include?(r) 
            # template<...>, enum class ...
            flush_token Token.new(:keyword, r)  # 'typename'もこのほうが自然
            @state = 1 if !['typename', 'class', 'struct', 'union', 'enum'].include?(r)
          elsif KW_TYPE.include?(r) || (@lang_name == 'objc' && OBJC_KW.include?(r))
            # const が後ろに続いたり, ポインタ型かもしれない.
            # 他方, int const MyVar = 10; ここで MyVar は variable.
            #   => 後ろから前方に向かって修正する.
            flush_token Token.new(:kw_type, r)
            @state = 1 if !['decltype', 'register'].include?(r)
          elsif KW_OP.include?(r)
            # これは型名ではない.
            flush_token Token.new(:kw_op, r)
            @state = 1
          else
            @typename << r if !@typename.include?(r)
            flush_token Token.new(:typename, r)
            @state = 1
          end
        when :annotation
          if OBJC_DIRECTIVES.include?(r[1..-1])
            flush_token Token.new(:keyword, r)
          else
            flush_token Token.new(:ident, r)
          end
          @state = 1
        when :operator
          case r
          when '<<'
            flush_token Token.new(:op_type, '<')
            @type_stack.unshift '>'
            flush_token Token.new(:op_type, '<')
            @type_stack.unshift '>'
          when '<'
            flush_token Token.new(:op_type, '<')
            @type_stack.unshift '>'
          when '>>'
            if @type_stack.index('>') # 1回目
              flush_token Token.new(:op_type, '>')
              @type_stack = @type_stack.drop( @type_stack.index('>') + 1 )
              if @type_stack.index('>') # 2回目
                flush_token Token.new(:op_type, '>')
                @type_stack = @type_stack.drop( @type_stack.index('>') + 1 )
              else
                flush_token Token.new(:operator, '>')
              end
            else
              flush_token Token.new(:operator, '>>')
            end
            @state = 1 if @type_stack.empty?
          when '>'
            if @type_stack.index('>')
              flush_token Token.new(:op_type, '>')
              @type_stack = @type_stack.drop( @type_stack.index('>') + 1 )
            else
              flush_token Token.new(:operator, '>')
            end
            @state = 1 if @type_stack.empty?
          when '*', '&'
            t = last_meaningful_token
            if t && t.symbol == :typename || t.symbol == :kw_type
              flush_token Token.new(:op_type, r) # 型の一部
            else
              flush_token Token.new(:operator, r)
            end
            @state = 1
          when ':'
            # class XXX: YYY
            flush_token Token.new(:operator, r)
            t = last_meaningful_token
            @state = 1 if !(t && t.symbol == :typename)
          when '=' # TODO: binary operator 全般にする
            t = last_meaningful_token
            if t && t.symbol == :typename
              # variable に戻す
              @typename.delete t.text
              t.symbol = :variable
            end
            flush_token Token.new(:operator, r)
            @state = 1
          else
            flush_token Token.new(:operator, r)
            @state = 1
          end
        else
          flush_token Token.new(sym, r)
          @state = 1 if sym != :space
        end
        return
      }
      # マッチなし
      ch = @scanner.getch
      if ch == '('
        t = last_meaningful_token
        if t && t.text == 'alignof'
          flush_token Token.new(:op_type, '(')
          @type_stack.unshift ')'
        elsif t && t.text == 'new'   # placement new構文
          flush_token Token.new(:other, ch)
          @state = 1
        else
          flush_token Token.new(:other, ch)
        end
        return
      elsif ch == ')'
        if @type_stack.index(ch)
          flush_token Token.new(:op_type, ')') 
          @type_stack = @type_stack.drop( @type_stack.index(ch) + 1 )
          @state = 1 if @type_stack.empty?
        else
          flush_token Token.new(:other, ch)
          @state = 1
        end
        return
      elsif ch == '{'
        @state = 1
      end

      @cur.symbol = :other
      @cur.text << ch
    end

  end # of class CLexer
end # of module Rb2HTML

end
