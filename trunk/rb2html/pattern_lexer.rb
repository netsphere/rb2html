# -*- coding:utf-8 -*-

require 'strscan'

class StringScanner
  def gets(bytes)
    raise TypeError if !bytes.is_a?(Integer)
    raise RangeError if bytes < 0

    r = peek(bytes)
    self.pos += bytes
    return r
  end
end


module Rb2HTML
  class Token
    attr_accessor :symbol, :text
    def initialize(sym, str)
      @symbol = sym
      @text = str
    end

    
    def ==(other)
      raise TypeError, "other is #{other.inspect}" if !other.is_a?(Token)
      return @symbol == other.symbol && @text == other.text
    end
    
    def inspect
      return "t(:#{@symbol.to_s}, #{@text.inspect})"
    end
  end

  
  class PatternLexer
  protected
    def initialize lang_name
      @lang_name = lang_name.downcase
    end

    # サブクラスでオーバーライドすること.
    def parse source, options
      raise ArgumentError, "source missing" if source.nil?
      raise TypeError if !options.is_a?(Hash)
      
      @scanner = StringScanner.new source
      @parsed = []
      @cur = Token.new(nil, '')
      @options = options
    end

  private

    # マッチしたらブロックを呼び出す
    # @return マッチしたとき true, しなかったとき false
    def match(patterns)
      patterns.each {|pat, sym, opt|
        if opt && opt[:only_after]
          last_kind = last_meaningful_token
          next if !last_kind
          if opt[:only_after].is_a?(Symbol)
            next if last_kind.symbol != opt[:only_after]
          elsif opt[:only_after].is_a?(Array)
            next if !opt[:only_after].include?(last_kind.symbol)
          else
            raise "internal error"
          end
        end

        r = @scanner.scan(pat)
        if r
          yield sym, r
          return true
        end
      }
      return false
    end

    # 必要に応じてサブクラスで上書きする
    def add_token token
      @parsed << token
      #if token.symbol != :space && token.symbol != :comment
      #  @last_kind = token.symbol
      #end
    end


    # 必要に応じてサブクラスで上書きする
    # @param rindex 後ろから何番目か. 1 で最後.
    # @return [Token] 最後のトークン or nil.
    def last_meaningful_token rindex = 1
      raise ArgumentError if rindex < 1
      
      if @cur.text != '' && ![:comment, :space].include?(@cur.symbol)
        add_token(Token.new(@cur.symbol, @cur.text))
        @cur.text = ''
      end
      
      ii = @parsed.length - 1
      token = nil
      (rindex - 1).downto(0) do
        while ii >= 0 && [:comment, :space].include?(@parsed[ii].symbol)
          ii -= 1
        end
        return nil if ii < 0

        token = @parsed[ii]
        ii -= 1
      end

      return token
    end

    
    # 1) @cur があるときはそれを @parsed に追加する。@curをクリアする.
    # 2) token を @parsed に追加する。
    #
    # @param [Token, nil] token 追加するトークン
    def flush_token token = nil
      if @cur.text != ''
        add_token(Token.new(@cur.symbol, @cur.text))
        @cur.text = ''
      end
      add_token(token) if token
    end
  end
end # of module Rb2HTML

