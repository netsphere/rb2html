# -*- coding:utf-8; mode:ruby -*-
# rb2html
# Copyright (c) 2001, 2004, 2006 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/
#     mailto:vzw00011@nifty.ne.jp

# 生成するシンボルを変更した場合は, rb2html-conf.rb ファイルの対応表も更新すること。


require 'rb2html/pattern_lexer'

module Rb2HTML
  class HaskellLexer < PatternLexer
    # Haskell 2010 で ':' が追加。
    # : で始まるシンボルは構築子 (コンストラクタ).
    # See http://stackoverflow.com/questions/19197339/haskell-data-constructor-meaning
    SYMBOL_EX_COLON = %r([!#\$\%&*+./<=>?@\\^|\-~])
    NOT_SYM = %r([^!#\$\%&*+./<=>?@\\^|~])  # -を除く

    # qualified name
    QMOD = %r(([A-Z][a-zA-Z0-9'_]*\.)+)

    S0_PATTERNS = [
      # float
      [/[0-9]+\.[0-9]+([eE][+-]?[0-9]+)?/, :numeric], # 0.0 or 1.0e-1
      [/[0-9]+[eE][+-]?[0-9]+/, :numeric],            # 1e1

      # integer
      [/0[xX][0-9a-fA-F]+/, :numeric],
      [/0[oO][0-7]+/, :numeric],
      [/[0-9]+/, :numeric],

      # ident
      # Haskell 2010: modid が先頭に付けられるようになった。
      [/#{QMOD}[a-z_][a-zA-Z0-9'_]*/, :variable],
      [/[a-z_][a-zA-Z0-9'_]*/, :variable],            # variables, type variables

      [/#{QMOD}[A-Z][a-zA-Z0-9'_]*/, :ctor],       
      [/[A-Z][a-zA-Z0-9'_]*/, :ctor],                 # constructors, type constructors, type classes

      # symbol or operator
      [/--+($|#{NOT_SYM}).*/, :comment],
      [/#{QMOD}#{SYMBOL_EX_COLON}(#{SYMBOL_EX_COLON}|\:)*/, :symbol], 
      [/#{SYMBOL_EX_COLON}(#{SYMBOL_EX_COLON}|\:)*/, :symbol],    # varsym     

      [/#{QMOD}:(#{SYMBOL_EX_COLON}|:)*/, :symbol], 
      [/:(#{SYMBOL_EX_COLON}|:)*/, :symbol],             # consym

      # comment
      [/\{-/, :comment_begin], # haskellのコメントはネストする

      # single character
      [/'(\\.|[^'])*'/, :string],  #'

      # string
      [/"(\\.|[^"])*"/, :string],  #"

      [/\r?\n|\r/, :eol],
      [/[ \t]+/, :space],
    ]

    SCOMMENT_PATTERNS = [
      [/\{-/, :comment_begin],
      [/-\}/, :comment_end],
    ]
  
    # Haskell 2010: foreign 追加.
    KEYWORDS = %w(case class  data    default deriving do     else
                  foreign
                if   import in      infix   infixl   infixr instance
                let  module newtype of      then     type   where    
                _)

    RESERVED_OPS = %w(.. : :: = \\ | <- -> @ ~ =>)


    def state_0
      match(S0_PATTERNS) {|sym, r|
        case sym
        when :variable
          # 型クラスが続く;
          #     data Bool = False | True deriving (Read, ...
          if ["class", "instance", "data", "type", "newtype",
              "deriving"].include?(r)
            flush_token Token.new(:keyword, r)
            @state = :st_type
            return
          end
          
          if KEYWORDS.include?(r)
            flush_token Token.new(:keyword, r)
          else
            flush_token Token.new(sym, r)
          end
        when :comment_begin
          flush_token
          @cur.symbol = :comment
          @cur.text = r
          @state = :st_comment
          @comment_back = :st_zero
          @comment_depth = 1
        when :symbol
          if r == "::"
            flush_token Token.new(:reserved_op, r)
            @state = :st_type
            return
          elsif r == "->"
            # ラムダ式の本体の場合もある  \ arg -> ...
            t = last_meaningful_token
            flush_token Token.new(:reserved_op, r)
            if t && t.symbol == :typename
              @state = :st_type
            end
            return
          end
          
          if RESERVED_OPS.include?(r)
            flush_token Token.new(:reserved_op, r)
          else
            flush_token Token.new(sym, r)
          end
        else
          flush_token Token.new(sym, r)
        end
        return
      }
      # マッチしなかったとき
      @cur.symbol = :other
      @cur.text << @scanner.getch
    end


    def state_comment
      match(SCOMMENT_PATTERNS) {|sym, r|
        @cur.text << r
        case sym
        when :comment_begin
          @comment_depth += 1
        when :comment_end
          @comment_depth -= 1
          if @comment_depth == 0
            if !@comment_back.is_a?(Symbol)
              raise TypeError, "internal error: #{@comment_back}"
            end
            @state = @comment_back
            @comment_back = nil
            flush_token
          end
        end
        return
      }
      @cur.text << @scanner.getch
    end


    # @override
    def last_meaningful_token
      if @cur.text != '' && ![:eol, :comment, :space].include?(@cur.symbol)
        add_token(Token.new(@cur.symbol, @cur.text))
        @cur.text = ''
        return @parsed.last
      end
      
      (@parsed.length - 1).downto(0) do |i|
        sym = @parsed[i].symbol
        next if sym == :eol || sym == :comment || sym == :space
        return @parsed[i]
      end
      return nil
    end

    
    # 型名
    def state_type
      match(S0_PATTERNS) {|sym, r|
        case sym
        when :eol
          # TODO: カッコで囲まれた型で閉じられていない場合, 継続行にする
          flush_token Token.new(:eol, r)
          t = last_meaningful_token
          if !(t && (t.text == "->" || t.text == "=>" || t.text == "deriving"))
            @state = :st_zero
          end
        when :ctor
          t = last_meaningful_token
          if t && (t.text == "=" || t.text == "|")
            # コンストラクタは型名ではない. その後ろの引数は型.
            flush_token Token.new(sym, r)  
          else
            flush_token Token.new(:typename, r)
          end
        when :variable
          if KEYWORDS.include?(r)
            flush_token Token.new(:keyword, r)
            # 型クラスが続く;
            #     data Point = Pt Double Double deriving Eq
            # 複数の型クラス ... deriving (Read, Show)
            if r != "deriving" 
              @state = :st_zero
            end
          else
            flush_token Token.new(sym, r)  # 型変数
          end
        when :comment_begin
          flush_token
          @cur.symbol = :comment
          @cur.text = r
          @state = :st_comment
          @comment_back = :st_type
          @comment_depth = 1
        when :symbol
          # 関数宣言での制約  sum :: Num a => [a] -> a
          # data MyException = MyException String deriving (Show) 
          if r == "->" || r == "=>" || r == '='
            flush_token Token.new(:reserved_op, r)
            return
          end
          
          if RESERVED_OPS.include?(r)
            flush_token Token.new(:reserved_op, r)
          else
            flush_token Token.new(sym, r)
          end
          @state = :st_zero
        else
          flush_token Token.new(sym, r)
        end
        return
      }
      # マッチしなかったとき
      @cur.symbol = :other
      @cur.text << @scanner.getch
    end

    
    # @override
    def parse source_, options = {}
      super(source_, options)
      
      @state = :st_zero
      while !@scanner.eos?
        raise TypeError, "internal error: #{@state}" if !@state.is_a?(Symbol)
        case @state
        when :st_zero
          state_0
        when :st_comment
          state_comment
        when :st_type
          state_type()
        else
          raise "internal error"
        end
      end
      flush_token
      return @parsed
    end
  end 
end

