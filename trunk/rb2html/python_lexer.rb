# -*- coding:utf-8 -*-
# rb2html
# Copyright (c) 2001, 2004, 2006 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/
#     mailto:vzw00011@nifty.ne.jp

require 'rb2html/pattern_lexer'

module Rb2HTML
  class PythonLexer < PatternLexer

    PYTHON_PATTERNS = [
    [/([0-9]*\.[0-9]+|[0-9]+\.)([eE][+-]?[0-9]+)?j?/, :numeric],
    [/0[xX][0-9A-Fa-f]+/, :numeric],

    [/[0-9]+[Llj]?/, :numeric],

    [/([ruRU]|[uU][rR])?'''(\\.|[^'])*'''/, :string],
    [/([ruRU]|[uU][rR])?"""(\\.|[^"])*"""/, :string],

    # TODO: raw stringの対応
    [/([ruRU]|[uU][rR])?'(\\.|[^'])*'/, :string],
    [/([ruRU]|[uU][rR])?"(\\.|[^"])*"/, :string],

    [/[a-zA-Z_][a-zA-Z0-9_]*/, :ident],
    [%r(\+|-|\*|\*\*|/|//|%|<<|>>|&|\||\^|~|<|>|<=|>=|==|!=), :operator],
    
    # Pythonの代入は文
    [%r(=|\+=|-=|\*=|/=|//=|%=|&=|\|=|\^=|>>=|<<=|\*\*=), :assignment],
    
    [/#.*/, :comment],
    [/\r\n?|\n/, :eol],
    [/[ \t]+/, :space],
    ]

    # exec, print は Python3で削除.
    KEYWORDS = %w(False None True
        and     as   del       for       is                raise    
        assert       elif      from      lambda  nonlocal  return   
        break        else      global    not               try      
        class        except    if        or                while  with  
        continue     exec      import    pass              yield    
        def          finally   in                )

    
    def parse source_, options = {}, &block
      super(source_, options)
      
      while !@scanner.eos?
        matched = match(PYTHON_PATTERNS) {|sym, r|
          #puts "text = #{r}, sym = #{sym}, pat = #{pat}"
          if sym == :ident
            if KEYWORDS.include?(r)
              flush_token Token.new(:keyword, r)
            else
              flush_token Token.new(sym, r)
            end
          else
            flush_token Token.new(sym, r)
          end
        }

        if !matched && (ch = @scanner.getch)
          @cur.symbol = :other
          @cur.text << ch
        end
      end

      flush_token() # @parsed << @cur
      return @parsed
    end
  end
end

