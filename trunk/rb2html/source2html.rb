# -*- coding:utf-8 -*-

# rb2html
# Copyright (c) 2001, 2004, 2006 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/
#     mailto:vzw00011@nifty.ne.jp

require 'rb2html/rb2html-sub'
require 'rb2html/rb2html-conf.rb'

module Rb2HTML

  module HelperFunctions
    # join() の逆関数
    # "hoge\n" => ["hoge", ""]
    def split_str(s, sep)
      raise TypeError if !s.is_a?(String)
      raise TypeError if !(sep.is_a?(String) || sep.is_a?(Regexp))
      
      if (i = s.index(sep))
        if sep.is_a?(String)
          return [i == 0 ? "" : s[0..(i-1)]] +
                 split_str(s[(i + sep.length)..-1], sep)
        else # Regexp
          return [$`] + split_str($', sep)
        end
      else
        return [s]
      end
    end
  end
    

  # 各プログラミング言語を整形するベースクラス
  class Source2Html
    include HelperFunctions
    
    def initialize layout, lexer, lang_name
      #raise TypeError if !layout.is_a?(String)
      #raise TypeError if !lexer.is_a?(String)
      raise TypeError if !lang_name.is_a?(String)
      
      @filename = ''
      @fo = layout
      @lexer = lexer
      @lang_name = lang_name.downcase
      @html_rules = RB2HTML_CONFIG[@lang_name] || raise("lang missing: #{lang_name}")
    end

    
    # <head>要素なども出力する。
    def output_file(io_or_str, start_lineno = 1)
      io, @filename = get_io_fn(io_or_str)
      @result = @fo.file_start(@filename)
      @result << @fo.start_formatted(start_lineno)
      format_body(io, start_lineno)
      @result << @fo.end_formatted() << @fo.file_end()
      return @result
    end

    # <pre>とその中身だけ出力する。
    # start_lineno  開始行番号。-1だと行番号を出力しない。
    def format_code(io_or_str, start_lineno = -1, options = {})
      io, @filename = get_io_fn(io_or_str)

      @result = @fo.start_formatted(start_lineno)
      format_body(io, start_lineno, options)
      @result << @fo.end_formatted()

      return @result.respond_to?(:html_safe) ? @result.html_safe : @result
    end

    
    private
    def get_io_fn(io_or_str)
      if defined?(io_or_str.read)
        io = io_or_str
      else
        require 'stringio'
        io = StringIO.new(io_or_str.chomp)
      end
      return [io, defined?(io.path) ? io.path : nil]
    end

    
    #  行頭文字を @result に追加
    def out_lno(first = false)
      @result << " </li>\n" if !first
      @lno += 1 if @lno >= 0
      @result << "<li>"
    end


    # @return [Hash] html属性ペア. sym が見つからなかったとき nil
    def find_rule(sym)
      opt = @html_rules
      begin
        opt[:rules].each {|rule|
          if (rule[0].is_a?(Symbol) && sym == rule[0]) ||
              (rule[0].is_a?(Array) && rule[0].include?(sym))
            return rule[1]
          end
        }
      end while opt[:base] && opt = RB2HTML_CONFIG[opt[:base]]
      return nil
    end

    
    # ソースコード部分を整形し, @result に追加する
    # @param [#read] io ソースコード入力
    def format_body(io, start_lineno, options = {})
      raise "no lexer" if !@lexer

      @lno = (start_lineno || 0) - 1
      ary = @lexer.parse(io.read, options)
      out_lno true
      begin_of_line = false
      ary.each {|token|
        raise TypeError, "token is #{token}" if !token.is_a?(Token)
        if begin_of_line
          out_lno() 
          begin_of_line = false
        end
        
        if token.text == "\n" || token.text == "\r\n"
          begin_of_line = true
          next
        end
        
        attrs = find_rule(token.symbol)
        
        # tokenは複数行にまたがることがある
        ts = split_str(token.text, /\r?\n/)
        ts.each_with_index {|line, i|
          if i != 0
            out_lno
            begin_of_line = false
          end
          if line == ""
            #begin_of_line = true
            next
          end

          if attrs
            tag = '<span'
            attrs.each {|k, v|
              tag << " #{k}=\"#{v}\""
            }
            tag << '>'
            @result << tag << Rb2HTML.html_escape(line) << '</span>'
          else
            @result << Rb2HTML.html_escape(line)
          end
        }
      } # each
      @result << "</li>\n"
    end
  end # of class Source2Html
end # of module Rb2HTML
