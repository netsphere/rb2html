# -*- coding:utf-8 -*-

# factory.rb と名前を合わせること.
RB2HTML_CONFIG = {
  '_normal_lang' => {
    :rules => [
      [:numeric, {:class => 'num'}],
      [:comment, {:class => 'comment'}],
      [:string, {:class => 'str'}],
      [:keyword, {:class => 'kw'}],
      [:operator, {:class => 'op'}],
    ],
  },

  'java' => {
    :base => '_normal_lang',
    :rules => [
      [:annotation, {'class' => 'anno'}],
      [:kw_class, {:class => 'kw'}],
      [:kw_typename, {:class => 'typename'}],
      [:typename, {:class => 'typename'}],
    ]
  },
  
  'objc' => {
    :base => '_normal_lang',
    :rules => [
      [:kw_type, {'class' => 'typename'}],
      [:typename, {'class' => 'typename'}],
      #[:op_type, {'class' => 'typename'}],  # クドい. 外す。
      [:kw_op, {:class => 'kw'}],
      [:cpp, {:class => 'cpp'}],
      #[:variable, {:class => 'var'}],    # 外す
      [:method, {:class => 'method'}],
    ]
  },

  'c++' => {
    :base => '_normal_lang',
    :rules => [
      [[:typename, :kw_type], {'class' => 'typename'}],
      #[:op_type, {'class' => 'typename'}],  # クドい. 外す。
      [:kw_op, {:class => 'kw'}],
      [:cpp, {:class => 'cpp'}],
      #[:variable, {:class => 'var'}],   # 外す.
    ]
  },
  
  'csharp' => {
    :base => '_normal_lang',
    :rules => [
      [[:typename, :kw_type], {'class' => 'typename'}],
      #[:op_type, {'class' => 'typename'}],  # クドい. 外す。
      [:kw_op, {:class => 'kw'}],
      [:cpp, {:class => 'cpp'}],
      #[:variable, {:class => 'var'}],   # 外す.
    ]
  },

  'javascript' => {
    :base => '_normal_lang',
    :rules => [
      [:tk_return, {:class => 'kw'}],
      [:reserved, {:class => 'reserved'}],
      [:regex, {:class => 'regex'}],
      [:tag_name, {'class'=>'tag'}],  # JSX (React)
    ],
  },

  'haskell' => {
    :base => '_normal_lang',
    :rules => [
      [:symbol, {'class' => 'op'}], # 演算子
      [:typename, {'class' => 'typename'}],
      [:reserved_op, {'class' => 'rsvop'}], 
      #[:variable, {:class => 'var'}],   ほとんどが variable で、クドい.
    ]
  },

  'ruby' => {
    :base => '_normal_lang',
    :rules => [
      [:symbol, {:class => 'sym'}],
      [:here_mark, {:class => 'str'}],
      [:class, {:class => 'kw'}],
      [:regex, {:class => 'regex'}],
      [[:class_name, :constant], {:class => 'typename'}],
      [:instance_variable, {:class => 'ivar'}],
    ]
  },

  'python' => {
    :base => '_normal_lang',
    :rules => [
      [:operator, {:class => 'op'}],
      [:assignment, {:class => 'assign'}]
    ]
  },

  'css' => {
    :rules => [
      [:comment, {'class'=>'comment'}],
      [:string, {'class'=>'str'}],
      [:property, {'class'=>'prop'}],
    ]
  },

  'html/xml' => {
    :base => 'ruby',  # ERBのために
    :rules => [
      [:eruby, {:class=>'erb'}],
      [:tag_name, {'class'=>'tag'}],
      [:att_name, {'class'=>'aname'}],
      [:att_value, {'class'=>'attval'}]
    ]
  }
}

