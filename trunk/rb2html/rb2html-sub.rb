# -*- coding:utf-8 -*-

# rb2html
# Copyright (c) 2001, 2004, 2006 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/
#     mailto:vzw00011@nifty.ne.jp

class String
  # 最初の文字を削除し、その文字を返す。レシーバ自身を変更する。
  # @return [String] 先頭文字.
  def shift
    r = self[0, 1]
    self[0, 1] = ''
    return r
  end
end

module Rb2HTML
  # エスケープする
  def html_escape(s)
    if s
      r = s.dup
      r.gsub! '&', '&amp;'
      r.gsub! '<', '&lt;'
      r.gsub! '>', '&gt;'
      r.gsub! '"', '&quot;'
      r.gsub! "'", '&#39;'
      r
    else
      nil
    end
  end
  module_function :html_escape
end

