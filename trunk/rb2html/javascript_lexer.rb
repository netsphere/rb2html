# -*- coding:utf-8 -*-
# rb2html
# Copyright (c) 2001, 2004, 2006 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/
#     mailto:vzw00011@nifty.ne.jp

if !$__RB2HTML_JS_LEXER__
$__RB2HTML_JS_LEXER__ = true
  
require 'rb2html/pattern_lexer'

module Rb2HTML
  class JavaScriptLexer < PatternLexer
    BASE_PATTERNS = [
      # float
      [/([0-9]+\.[0-9]*|\.[0-9]+)([eE][+-]?[0-9]+)?/, :numeric], # 0. or .0
      [/[0-9]+[eE][+-]?[0-9]+/, :numeric],                       # 1e1

      # integer
      [/0[xX][0-9a-fA-F]+/, :numeric],
      [/[0-9]+/, :numeric],

      # comment
      [%r(/\*(.|\n)*?\*/), :comment], # ネストなし
      [%r(//.*), :comment],          # 行末まで

      # string  改行は含められない
      [/"(\\.|[^"])*"/, :string],  #"
      [/'(\\.|[^'])*'/, :string],  #'
      [/`(\\.|[^`])*`/, :string],  # テンプレート. TODO: ${式}
    ]

    JS_PATTERNS = BASE_PATTERNS + [
      # ObjectLiteral
      [/[a-zA-Z_][a-zA-Z_0-9]*[ \t]*:/, :member_key,
                        {:only_after => [:start_obj, :comma]} ],

      # ident   正しくはUnicode文字の多くが使える
      [/[a-zA-Z_][a-zA-Z_0-9]*/, :ident],

      # ObjectLiteral
      [/\{/, :start_obj,
            {:only_after => [:operator, :comma, :bracket_open, :kv_sep,
                             :tk_return]} ],
        
      # e4x, JSX (React)
      [/<[A-Za-z_][A-Za-z0-9._-]*/, :start_e4x,
            {:only_after => [:operator, :comma, :bracket_open, :kv_sep,
                             :tk_return]} ],

      # operator
      [%r(\+=|-=|\*=|&=|\|=|\^=|%=|<<=|>>=|>>>=), :operator],
      [/===|!==|==|<=|>=|!=|&&|\|\|/, :operator],
      [/\+\+|--/, :op_unary],
      [/<<|>>|>>>/, :operator],
      # 3項演算子のコロンはこちら.
      [%r(=|>|<|!|~|\?|:|\+|-|\*|&|\||\^|%), :operator],
      [%r(/=|/), :operator,
                     {:only_after => [:numeric, :ident, :string, :regex]}],

      # /* or // はコメントとしてよい。空の正規表現は/(?:)/と書く。
      [%r(/(\\.|[^/])+/[A-Za-z]?), :regex],
      [/\//, :operator], 

      [/,/, :comma],
      [/[(\[]/, :bracket_open],
      [/[ \t]*\r?\n|\r/, :space,
            {:only_after => [:operator, :start_obj, :comma, :bracket_open, :kv_sep]}],
      [/\r?\n|\r/, :eol],
      [/[ \t]+/, :space],
    ]

=begin
    # TODO: もそっとまともに。
    # http://www.ietf.org/rfc/rfc4627.txt
    SJSON_PATTERNS = BASE_PATTERNS + [
      # ident   jsonのなかでは文字列。
      [/([a-zA-Z_][a-zA-Z_0-9]*)([ \t]*):/, :member_key],

      [/:/, :colon],
      [/\s+/, :space],
      [/\}/, :end_json],
    ]
=end
       
    # ReservedWord ::
    #     Keyword
    #     FutureReservedWord
    #     NullLiteral
    #     BooleanLiteral
    # 'return' だけ特別扱い. TK_RETURN
    KEYWORDS = %W(break    else       new    var
                  case     finally    return void
                  catch    for        switch while
                  continue function   this   with
                  debugger if         throw  
                  default  in         try
                  delete   instanceof typeof
                  do       
                  null true false
               # ES2017 (8th) までに追加.
                  async await  
                  class   const   export   extends   import super yield
                  let     static  )
    # await はキーワードだが, async は文脈依存キーワード。
    # クラスのメソッド名として new() や delete() なども自由に使える。
    # => 基本はキーワードということにしておいて、適宜, ident 扱いにする.
    
    # 3rd Ed. (Dec 1999) は Javaのキーワード (?) が reserved になっていた。
    # だいぶ減った.
    RESERVED = %W(enum     
                  implements   interface   package   private
                  protected    public)

    
    def state_0
      matched = match(JS_PATTERNS) {|sym, r|
        #puts "text = #{r}, sym = #{sym}, pat = #{pat}"     # DEBUG
        case sym
        when :start_obj
          flush_token Token.new(:start_obj, r)
          #@state = :st_json
        when :member_key
          r =~ /([a-zA-Z_][a-zA-Z_0-9]*)([ \t]*):/
          flush_token Token.new(:string, $1)
          flush_token Token.new(:space, $2) if $2 && $2 != ''
          flush_token Token.new(:kv_sep, ':')
        when :start_e4x
          @scanner.unscan
          @state = :st_e4x
        when :ident
          # .catch(...) はキーワードではない
          t = last_meaningful_token
          if t && t.text == '.'
            flush_token Token.new(:method, r)
          elsif t && ['class', 'extends', 'new'].include?(t.text)
            flush_token Token.new(:typename, r)
          else
            if KEYWORDS.include?(r)
              if r == 'return'
                flush_token Token.new(:tk_return, r)
              else
                flush_token Token.new(:keyword, r)
              end
            elsif RESERVED.include?(r)
              flush_token Token.new(:reserved, r)
            else
              flush_token Token.new(sym, r)
            end
          end
        when :bracket_open
          if r == '('
            # class F { new() { } }
            # class F { delete() { } }
            # async は async () => {...} と書く場合があるので、曖昧
            t = last_meaningful_token
            if t && ['new', 'delete'].include?(t.text)
              t.symbol = :method
            end
          end
          flush_token Token.new(sym, r) # :bracket_open
        else
          flush_token Token.new(sym, r)
        end
      }

      if !matched
        @last_kind = @cur.symbol = :other
        @cur.text << @scanner.getch
      end
    end

    
=begin    
    # ObjectLiteral :
    #     '{' '}' |
    #     '{' PropertyAssignment ( ',' PropertyAssignment )* [','] '}'
    # 中略
    # PropertyName :
    #     IdentifierName |
    #     StringLiteral |
    #     NumericLiteral
    def state_json
      # TODO: state の stack
      matched = match(SJSON_PATTERNS) {|sym, r|
        if sym == :end_json
          flush_token Token.new(sym ,r)
          @state = :st_zero
        else
          flush_token Token.new(sym, r)
        end
      }

      if !matched
        @last_kind = @cur.symbol = :other
        @cur.text << @scanner.getch
      end
    end
=end
    
    def state_e4x
      require 'rb2html/html_parse'
      ary, rest = HTMLRawParser.new('xml').parse @scanner.rest, true
      ary.each {|e|
        flush_token e
      }
      @scanner.string = rest
      @state = :st_zero
    end

    def parse source_, options = {}, &block
      super(source_, options)
      
      @state = :st_zero
      @last_kind = :begin_of_source
      while !@scanner.eos?
        if !@state.is_a?(Symbol)
          raise TypeError, "internal error: @state must be Symbol"
        end
        
        case @state
        when :st_zero
          state_0
        when :st_json
          state_json
        when :st_e4x
          state_e4x
        else
          raise "internal error: unknown state: #{@state}"
        end
      end
      flush_token
      return @parsed
    end

  end # of class JavaScriptLexer
end

end

