# -*- coding:utf-8 -*-

require '../rb2html/python_lexer.rb'
require 'test/unit'

def t(sym, text)
  return Rb2HTML::Token.new(sym, text)
end

class PythonLexerTestCase < Test::Unit::TestCase
  SP = t(:space, ' '); DOT = t(:other, '.'); EOL = t(:eol, "\n")

  def setup
    @lex = Rb2HTML::PythonLexer.new
  end
  
  def test1
    d = @lex.parse <<'EOF'
import os
filename=os.environ.get('PYTHONSTARTUP')
if filename and os.path.isfile(filename):
    execfile(filename)
EOF
    assert_equal([
      t(:keyword, 'import'), SP, t(:ident, 'os'), EOL,
      t(:ident, 'filename'), t(:assignment, '='),
      t(:ident, 'os'), DOT, t(:ident, 'environ'), DOT, t(:ident, 'get'),
      t(:other, '('), t(:string, "'PYTHONSTARTUP'"), t(:other, ')'), EOL,
      t(:keyword, "if"), SP, t(:ident, "filename"), SP, t(:keyword, "and"), SP,
      t(:ident, "os"), DOT, t(:ident, "path"), DOT, t(:ident, "isfile"),
      t(:other, "("), t(:ident, "filename"), t(:other, "):"), EOL,
      t(:space, "    "), t(:ident, "execfile"), t(:other, "("), t(:ident, "filename"),
      t(:other, ")"), EOL], d)
  end

  
  # 行末に空白があると、一行、余計な空白行になる？
  # => /\s+/ は改行文字にもマッチする。:eol を別に生成する場合は, /[ \t]+/にする.
  def test_tail_space
    src = <<'EOF'
x = int( input("数字>>> ") ) 
if x > 5: 
    print("入力された数字は5より大きい。")
EOF
    d = @lex.parse src
    assert_equal([
 t(:ident, "x"),
 t(:space, " "),
 t(:assignment, "="),
 t(:space, " "),
 t(:ident, "int"),
 t(:other, "("),
 t(:space, " "),
 t(:ident, "input"),
 t(:other, "("),
 t(:string, "\"数字>>> \""),
 t(:other, ")"),
 t(:space, " "),
 t(:other, ")"),
 t(:space, " "),
 t(:eol, "\n"),
 t(:keyword, "if"),
 t(:space, " "),
 t(:ident, "x"),
 t(:space, " "),
 t(:operator, ">"),
 t(:space, " "),
 t(:numeric, "5"),
 t(:other, ":"),
 t(:space, " "),
 t(:eol, "\n"),
 t(:space, "    "),
 t(:keyword, "print"),
 t(:other, "("),
 t(:string, "\"入力された数字は5より大きい。\""),
 t(:other, ")"),
 t(:eol, "\n")
                 ], d)
  end
  
end
