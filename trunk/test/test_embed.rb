
# 埋め込みのテスト

require "rb2html/factory"
require 'test/unit'

class EmbedTestCase < Test::Unit::TestCase
  
  def test_embed
    r2h = Rb2HTML::Factory.get_formatter 'ruby'
    print r2h.format_code(<<EOF, 21)
# here is ruby script.
def foo
  print %w(a b c) '123' "abc" / 2
end
EOF
  end
end
