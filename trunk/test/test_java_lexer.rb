# -*- coding:utf-8 -*-

require '../rb2html/java_lexer.rb'
require 'test/unit'

def t(sym, text)
  return Rb2HTML::Token.new(sym, text)
end

class JavaLexerTestCase < Test::Unit::TestCase
  SP = t(:space, ' '); EOL = t(:eol, "\n")

  def setup
    @lex = Rb2HTML::JavaLexer.new
  end
  
  # 数値
  def test1
    d = @lex.parse %q(3.14f 0777L null 0xDadaCafe '\\')
    assert_equal([
      t(:numeric, '3.14f'), SP, 
      t(:numeric, '0777L'), SP,
      t(:keyword, 'null'), SP,
      t(:numeric, '0xDadaCafe'), SP,
      t(:string, %q('\\'))
    ], d)
  end

  # コメント
  def test_comment
    d = @lex.parse %q(3/2/* this comment /* // /** ends here: */)
    assert_equal([
      t(:numeric, '3'), t(:operator, '/'), t(:numeric, '2'),
      t(:comment, %q(/* this comment /* // /** ends here: */))], d)
  end

  # 最後のトークンが出力されないケースがある？
  def test_last
    text = <<'EOS'
document.write('xxx');
EOS

    # まず字句解析器
    d = @lex.parse text 
    assert_equal [
 t(:ident, "document"),
 t(:other, "."),
 t(:ident, "write"),
 t(:other, "("),
 t(:string, "'xxx'"),
 t(:other, ")"),
 t(:semicolon, ";"),
 t(:space, "\n")
    ], d

    # フォーマッタ
    require 'rb2html/factory'
    f = Rb2HTML::Factory.get_formatter 'java'
    assert_kind_of(Rb2HTML::Source2Html, f)

    result = f.format_code text
    assert_equal('<div class="source"><ol class="nolineno">
<li>document.write(<span class="str">&#39;xxx&#39;</span>);</ol></div>', 
                 result)
  end

  # ジェネリクス
  def test_generics
    d = @lex.parse 'new LinkedList<LinkedList<String>>();'
    assert_equal [
 t(:kw_class, "new"),
 t(:space, " "),
 t(:typename, "LinkedList"),
 t(:template_open, "<"),
 t(:typename, "LinkedList"),
 t(:template_open, "<"),
 t(:typename, "String"),
 t(:template_close, ">"),
 t(:template_close, ">"),
 t(:other, "()"),
 t(:semicolon, ";")
    ], d
  end

  def test_generics2
    d = @lex.parse <<EOF
{
    List<LinkedList<String>> list = new LinkedList<LinkedList<String>>();
    for (int i = 0; i < args.length; i++) {
EOF
    assert_equal [
 t(:block_open, "{"), t(:space, "\n    "),
 t(:typename, "List"), t(:template_open, "<"), t(:typename, "LinkedList"),
     t(:template_open, "<"), t(:typename, "String"), t(:template_close, ">"),
     t(:template_close, ">"), t(:space, " "), t(:ident, "list"),
     t(:space, " "), t(:operator, "="), t(:space, " "), t(:kw_class, "new"),
     t(:space, " "), t(:typename, "LinkedList"), t(:template_open, "<"),
     t(:typename, "LinkedList"), t(:template_open, "<"), t(:typename, "String"),
     t(:template_close, ">"), t(:template_close, ">"),
     t(:other, "()"), t(:semicolon, ";"), t(:space, "\n    "),
 t(:keyword, "for"), t(:space, " "), t(:other, "("), t(:kw_typename, "int"),
     t(:space, " "), t(:ident, "i"), t(:space, " "), t(:operator, "="),
     t(:space, " "), t(:numeric, "0"), t(:semicolon, ";"), t(:space, " "),
     t(:ident, "i"),   # ここか難しい。
     t(:space, " "),
     t(:operator, "<"),
     t(:space, " "),
     t(:ident, "args"),
     t(:other, "."), t(:ident, "length"), t(:semicolon, ";"), t(:space, " "),
     t(:ident, "i"), t(:operator, "++"), t(:other, ")"), t(:space, " "),
     t(:block_open, "{"), t(:space, "\n")
    ], d
  end
end

