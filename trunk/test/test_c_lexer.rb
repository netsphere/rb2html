# -*- coding:utf-8 -*-

require '../rb2html/c_lexer.rb'
require 'test/unit'

def t(sym, text)
  return Rb2HTML::Token.new(sym, text)
end

class CLexerTestCase < Test::Unit::TestCase
  SP = t(:space, ' '); EOL = t(:eol, "\n")

  def setup
    @lex = Rb2HTML::CLexer.new
  end

  # 数値
  def test1
    d = @lex.parse %q(3.14f 0777L 0xDadaCafe '\\')
    assert_equal([
      t(:numeric, '3.14f'), SP, 
      t(:numeric, '0777L'), SP,
      t(:numeric, '0xDadaCafe'), SP,
      t(:string, %q('\\'))
    ], d)
  end

  # コメント
  def test_comment
    d = @lex.parse %q(3/2/* this comment /* // /** ends here: */)
    assert_equal([
      t(:numeric, '3'), t(:operator, '/'), t(:numeric, '2'),
      t(:comment, %q(/* this comment /* // /** ends here: */))], d)
  end

  # 最後のトークンが出力されないケースがある？
  def test_last
    text = <<'EOS'
document.write("xxx\n");
EOS

    # まず字句解析器
    d = @lex.parse text 
    assert_equal [
 t(:ident, "document"),
 t(:other, "."),
 t(:ident, "write"),
 t(:other, "("),
 t(:string, "\"xxx\\n\""),
 t(:other, ");"),
 t(:space, "\n")
    ], d

    # フォーマッタ
    require 'rb2html/factory'
    f = Rb2HTML::Factory.get_formatter 'c'
    assert_kind_of(Rb2HTML::Source2Html, f)

    result = f.format_code text
    assert_equal('<div class="source"><ol class="nolineno">
<li>document.write(<span class="str">&quot;xxx\\n&quot;</span>);</ol></div>', 
                 result)
  end

end

