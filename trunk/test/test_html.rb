
require "../rb2html/html_parse.rb"
require 'test/unit'

def t(sym, text)
  return Rb2HTML::Token.new(sym, text)
end

class HTMLTestCase < Test::Unit::TestCase
  def setup
    @lex = Rb2HTML::HTMLRawParser.new
  end

  def test_erb
    sp = t(:space, ' '); eq = t(:eq, '=')
    d = @lex.parse('<div id=1><% class Foo; end %></div>')
    assert_equal([
      t(:stago, '<'), t(:tag_name, 'div'), sp, t(:att_name, 'id'), eq,
      t(:att_value, '1'), t(:stagc, '>'), t(:eruby, '<% '),
      # ruby script
      t(:class, 'class'), sp, t(:class_name, 'Foo'), t(:delimiter, ';'), sp,
      t(:keyword, 'end'), sp,
      t(:eruby, '%>'),
      t(:etago, '</'),
      t(:tag_name, 'div'), t(:etagc, '>')], d)
  end

  def test_etag
    d = @lex.parse('</fo:block>')
    assert_equal([
      t(:etago, '</'), t(:tag_name, 'fo:block'), t(:etagc, '>')], d)

    d = @lex.parse('</block   >')
    assert_equal([
      t(:etago, '</'), t(:tag_name, 'block'), t(:space, '   '), t(:etagc, '>')], d)
  end

  # <script>内のJavaScript
  def test_script
    d = @lex.parse('<script type="text/javascript">do {
}</script>')
    assert_equal([
  t(:stago, "<"),
  t(:tag_name, "script"),
  t(:space, " "),
  t(:att_name, "type"),
  t(:eq, "="),
  t(:att_value, "\"text/javascript\""),
  t(:stagc, ">"),
  t(:keyword, "do"),
  t(:space, " "),
  t(:other, "{"),
  t(:eol, "\n"),
  t(:other, "}"),
  t(:etago, "</"),
  t(:tag_name, "script"),
  t(:etagc, ">")
    ], d)
  end

  def test_a
    h = Rb2HTML::HTMLRawParser.new
    pa = h.parse <<EOF
<html>
<foo a=b c d="ho'ge">aa</bar><e/><f z::z>< <0> >
EOF
  pa.each {|t|
    puts "#{t.symbol}, '#{t.text}'"
  }
  end

end
