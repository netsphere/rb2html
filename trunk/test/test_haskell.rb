# -*- coding:utf-8 -*-

# Haskell のソース

require "../rb2html/haskell_lexer.rb"
require 'test/unit'

def t(sym, text)
  return Rb2HTML::Token.new(sym, text)
end


# 全体のソースコード。これを抜粋する.
=begin
{-# LANGUAGE OverloadedStrings #-} 
 
import Data.ByteString       as BS 
import Data.ByteString.Char8 as BSC 
import System.IO 
 
main :: IO () 
main = do 
    Prelude.putStr "何か入力> " 
    hFlush stdout 
    str <- BS.getLine 
    BSC.putStrLn $ "str = " `BS.append` str 
    Prelude.putStrLn $ (show $ BS.length str) ++ " bytes"
=end

class HaskellLexerTestCase < Test::Unit::TestCase
  def test_1
    lex = Rb2HTML::HaskellLexer.new
    d = lex.parse <<'EOF'
{-# LANGUAGE OverloadedStrings #-}
import Data.ByteString       as BS
main :: IO ()
main = do
    Prelude.putStr "何か入力> "
    str <- BS.getLine
    BSC.putStrLn $ "str = " `BS.append` str
    Prelude.putStrLn $ (show $ BS.length str) ++ " bytes"
EOF

    assert_equal [
 t(:comment, "{-# LANGUAGE OverloadedStrings #-}"),
 t(:eol, "\n"),
 t(:keyword, "import"),
 t(:space, " "),
 t(:ctor, "Data.ByteString"),
 t(:space, "       "),
 t(:variable, "as"),   # TODO: keyword. 文脈依存
 t(:space, " "),
 t(:ctor, "BS"),
 t(:eol, "\n"),
 t(:variable, "main"),
 t(:space, " "),
 t(:reserved_op, "::"),
 t(:space, " "),
 t(:typename, "IO"),
 t(:space, " "),
 t(:other, "()"),
 t(:eol, "\n"),
 t(:variable, "main"),
 t(:space, " "),
 t(:reserved_op, "="),
 t(:space, " "),
 t(:keyword, "do"),
 t(:eol, "\n"),
 t(:space, "    "),
 t(:variable, "Prelude.putStr"),
 t(:space, " "),
 t(:string, "\"何か入力> \""),
 t(:eol, "\n"),
 t(:space, "    "),
 t(:variable, "str"),
 t(:space, " "),
 t(:reserved_op, "<-"),
 t(:space, " "),
 t(:variable, "BS.getLine"),
 t(:eol, "\n"),
 t(:space, "    "),
 t(:variable, "BSC.putStrLn"),
 t(:space, " "),
 t(:symbol, "$"),
 t(:space, " "),
 t(:string, "\"str = \""),
 t(:space, " "),
 t(:other, "`"),
 t(:variable, "BS.append"),
 t(:other, "`"),
 t(:space, " "),
 t(:variable, "str"),
 t(:eol, "\n"),
 t(:space, "    "),
 t(:variable, "Prelude.putStrLn"),
 t(:space, " "),
 t(:symbol, "$"),
 t(:space, " "),
 t(:other, "("),
 t(:variable, "show"),
 t(:space, " "),
 t(:symbol, "$"),
 t(:space, " "),
 t(:variable, "BS.length"),
 t(:space, " "),
 t(:variable, "str"),
 t(:other, ")"),
 t(:space, " "),
 t(:symbol, "++"),
 t(:space, " "),
 t(:string, "\" bytes\""),
 t(:eol, "\n")]   , d
  end


  def test_typename
    lex = Rb2HTML::HaskellLexer.new
    d = lex.parse <<'EOF'
class DataPacket a where
    toStrictBS :: a -> Strict.ByteString
    fromStrictBS :: Strict.ByteString -> a

instance DataPacket Strict.ByteString where
  toStrictBS = id

openBoundUDPPort :: String -> Int -> IO Socket
openBoundUDPPort uri port = do
EOF

    assert_equal [
 t(:keyword, "class"),
 t(:space, " "),
 t(:typename, "DataPacket"),
 t(:space, " "),
 t(:variable, "a"),
 t(:space, " "),
 t(:keyword, "where"),
 t(:eol, "\n"),
 t(:space, "    "),
 t(:variable, "toStrictBS"),
 t(:space, " "),
 t(:reserved_op, "::"),
 t(:space, " "),
 t(:variable, "a"),
 t(:space, " "),
 t(:reserved_op, "->"),
 t(:space, " "),
 t(:typename, "Strict.ByteString"),
 t(:eol, "\n"),
 t(:space, "    "),
 t(:variable, "fromStrictBS"),
 t(:space, " "),
 t(:reserved_op, "::"),
 t(:space, " "),
 t(:typename, "Strict.ByteString"),
 t(:space, " "),
 t(:reserved_op, "->"),
 t(:space, " "),
 t(:variable, "a"),
 t(:eol, "\n"),
 t(:eol, "\n"),
 t(:keyword, "instance"),
 t(:space, " "),
 t(:typename, "DataPacket"),
 t(:space, " "),
 t(:typename, "Strict.ByteString"),
 t(:space, " "),
 t(:keyword, "where"),
 t(:eol, "\n"),
 t(:space, "  "),
 t(:variable, "toStrictBS"),
 t(:space, " "),
 t(:reserved_op, "="),
 t(:space, " "),
 t(:variable, "id"),
 t(:eol, "\n"),
 t(:eol, "\n"),
 t(:variable, "openBoundUDPPort"),
 t(:space, " "),
 t(:reserved_op, "::"),
 t(:space, " "),
 t(:typename, "String"),
 t(:space, " "),
 t(:reserved_op, "->"),
 t(:space, " "),
 t(:typename, "Int"),
 t(:space, " "),
 t(:reserved_op, "->"),
 t(:space, " "),
 t(:typename, "IO"),
 t(:space, " "),
 t(:typename, "Socket"),
 t(:eol, "\n"),
 t(:variable, "openBoundUDPPort"),
 t(:space, " "),
 t(:variable, "uri"),
 t(:space, " "),
 t(:variable, "port"),
 t(:space, " "),
 t(:reserved_op, "="),
 t(:space, " "),
 t(:keyword, "do"),
 t(:eol, "\n")
    ], d
  end
end

