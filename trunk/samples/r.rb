=begin
  Rubyスクリプトのサンプル
  リテラル（数値、文字列、配列、シンボル、正規表現）、キーワード、コメント
=end
  
def hoge
  print <<EOF, <<'EOF'
hoge # hoge
EOF
foo # bar # baz
EOF
  %w(1 2 3).each {|s| print s}
  while line = gets
    break if line =~ /end/
  end
  p 123.5, :[]=, %r!foo!, %s(sym)
end

#main
hoge()
