
# リテラルのテスト

require "../rb2html/ruby_lexer.rb"

s = <<'EOF'
123
123.4
1.2e-3
0xffff
?a
?\C-a
`date`
%x{date}   # コマンド
%r|fo#{'hoge'}o|ie  # 正規表現
%w<1 2 3>  # 配列式

%!string!  # 文字列
%Q{string} # 文字列
%q!string! # 文字列
EOF

lex = Rb2HTML::RubyLexer.new
r = lex.parse(s)
r.each {|t|
  print "#{t.symbol}, #{t.text}\n"
}
