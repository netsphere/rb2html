
def test0
  require "rdoc/parsers/parse_rb"
  lex = RubyLex.new('p(1, 2, 3)')
  while tk = lex.token
    p tk
  end
end

def test1
  require "irb/ruby-lex.rb"
  require "stringio"
  lex = RubyLex.new
  lex.set_input StringIO.new('p(1, 2, 3)')
  while tk = lex.token
    p tk
  end
end

test1
