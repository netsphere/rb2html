
require '../rb2html/python_lexer.rb'
require 'test/unit'

def t(sym, text)
  return Rb2HTML::Token.new(sym, text)
end

class PythonLexerTestCase < Test::Unit::TestCase
  def setup
    @lex = Rb2HTML::PythonLexer.new
  end
  
  def test1
    sp = t(:space, ' '); dot = t(:other, '.'); eol = t(:eol, "\n")

    d = @lex.parse <<'EOF'
import os
filename=os.environ.get('PYTHONSTARTUP')
if filename and os.path.isfile(filename):
    execfile(filename)
EOF
    assert_equal([
      t(:keyword, 'import'), sp, t(:ident, 'os'), eol,
      t(:ident, 'filename'), t(:assignment, '='),
      t(:ident, 'os'), dot, t(:ident, 'environ'), dot, t(:ident, 'get'),
      t(:other, '('), t(:string, "'PYTHONSTARTUP'"), t(:other, ')'), eol,
      t(:keyword, "if"), sp, t(:ident, "filename"), sp, t(:keyword, "and"), sp,
      t(:ident, "os"), dot, t(:ident, "path"), dot, t(:ident, "isfile"),
      t(:other, "("), t(:ident, "filename"), t(:other, "):"), eol,
      t(:space, "    "), t(:ident, "execfile"), t(:other, "("), t(:ident, "filename"),
      t(:other, ")"), eol], d)
  end
end
