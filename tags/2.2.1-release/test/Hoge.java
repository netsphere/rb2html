/*
  Javaソースのサンプル
*/

import java.awt.Graphics;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.applet.Applet;

public class Hoge extends Applet {
  public void paint(Graphics g) {
    g.drawString("はろーわーるど", 25, 50);
    g.drawString("Javaバージョン: "
                 + System.getProperty("java.version"), 25, 70);
  }
  public static void main(String[] args) { // アプリケーションとしても使えるようにする
    Frame f = new Frame("Hoge");
    f.add(new Hoge());
    f.setSize(200, 150);
    f.addWindowListener(new WindowAdapter() {
        public void windowClosing(WindowEvent e) {
            System.exit(0);
        }});
    f.setVisible(true);
  }
}
