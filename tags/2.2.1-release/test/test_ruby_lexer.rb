
require "../rb2html/ruby_lexer.rb"
require 'test/unit'

def t(sym, text)
  return Rb2HTML::Token.new(sym, text)
end

class RubyLexerTestCase < Test::Unit::TestCase
  def setup
    @lex = Rb2HTML::RubyLexer.new
  end
  
  def test0
    sp = t(:space, ' '); comma = t(:delimiter, ','); eol = t(:eol, "\n")
    eq = t(:operator, '=')

    # 数値リテラル、文字列、正規表現
    d = @lex.parse <<'EOScript'
p 1.1e-2, 0x0aaff/2,'fo\'o\\'
ABC=/abc/m
EOScript
    
    assert_equal([
    t(:ident, 'p'), sp, t(:numeric, '1.1e-2'), comma, sp,
    t(:numeric, '0x0aaff'), t(:operator, '/'), t(:numeric, '2'), comma,
    t(:string, "'fo\\'o\\\\'"), eol,
    t(:constant, "ABC"), t(:operator, '='), t(:regex, '/abc/m'), eol],
    d)
    
    # キーワード、メソッド、定数、インスタンス変数、クラス変数、=begin...=end
    d = @lex.parse <<'EOScript'
def foo end;p foo.is_a?(Class)
@b=@@c=Const="hog
e"  # comment
=begin foobar
here is comment
=end
p defined?(foo.def),$1
EOScript
    
    assert_equal([
    t(:keyword, 'def'), sp, t(:ident, 'foo'), sp, t(:keyword, 'end'), t(:delimiter, ';'),
    t(:ident, 'p'), sp, t(:ident, 'foo'), t(:dot, '.'), t(:method, 'is_a?'),
    t(:bracket_open, '('), t(:constant, 'Class'), t(:bracket_close, ')'), eol,
    t(:instance_variable, "@b"), eq, t(:class_variable, "@@c"), eq, t(:constant, "Const"),
    eq, t(:string, "\"hog\ne\""), t(:space, "  "), t(:comment, "# comment"),
    t(:eol, "\n"),
    t(:comment, "=begin foobar\nhere is comment\n=end\n"),
    t(:ident, "p"), sp, t(:keyword, "defined?"), t(:bracket_open, "("), t(:ident, "foo"),
    t(:dot, "."), t(:method, "def"), t(:bracket_close, ")"), comma, t(:global_variable, "$1"),
    eol], d)

    # ヒアドキュメント、シンボル、%表記
    d = @lex.parse <<'EOScript'
p <<EOF,<<'HOGE',:hoge=,:[]
text
EOF
hogehoge
HOGE
a=[?c]+%w(foo bar)
EOScript
    assert_equal([
    t(:ident, 'p'), sp, t(:here_mark, '<<EOF'), comma, t(:here_mark, "<<'HOGE'"), comma,
    t(:symbol, ':hoge='), comma, t(:symbol, ':[]'), eol,
    t(:string, "text\nEOF"), eol, t(:string, "hogehoge\nHOGE"), eol,
    t(:ident, 'a'), eq, t(:bracket_open, '['), t(:numeric, '?c'), t(:bracket_close, ']'),
    t(:operator, '+'), t(:array, '%w(foo bar)'), eol], d)
  end
  
  # ヒアドキュメント <<-形式
  def test1
    sp = t(:space, ' '); eol = t(:eol, "\n")
    
    d = @lex.parse <<'EOScript'
p <<-EOS
  text.
  EOS
EOScript
    assert_equal([
    t(:ident, 'p'), sp, t(:here_mark, '<<-EOS'), eol, t(:string, "  text.\n  EOS"), eol],
    d)
  end
  
  # %形式
  def test2
    comma = t(:delimiter, ",")

    d = @lex.parse('p %Q(foo),%q[bar],%r!hoge!,%s<funi>')
    assert_equal([
    t(:ident, 'p'), t(:space, ' '), t(:string, '%Q(foo)'), comma,
    t(:string, '%q[bar]'), comma,
    t(:regex, '%r!hoge!'), comma,
    t(:symbol, '%s<funi>')], d)
  end

  # 文字列、正規表現の式展開
  def test3
    eq = t(:operator, '='); eol = t(:eol, "\n")

    d = @lex.parse File.read('data/ruby_3.rb')
    assert_equal([
    t(:ident, 's'), eq, t(:string, '"abc #{t = ""; (1..3).each {|i| t << i}; t} bar"'),
    eol,
    t(:ident, 't'), eq, t(:regex, '/foo#{u = /abc/; (1..3).each {"a"}}bar/'), eol], d)
  end

  # '/'  正規表現リテラル or 演算子
  def test4
    d = @lex.parse <<'EOScript'
p m/2#/
p n /2#/
EOScript
    
    mp = t(:ident, 'p'); sp = t(:space, ' '); eol = t(:eol, "\n")
    assert_equal([
    mp, sp, t(:ident, 'm'), t(:operator, '/'), t(:numeric, '2'),
    t(:comment, '#/'), eol,
    mp, sp, t(:ident, 'n'), sp, t(:regex, '/2#/'), eol], d)
  end

  # '%'  演算子 or %記法
  def test5
    sp = t(:space, ' '); eol = t(:eol, "\n")

    d = @lex.parse <<'EOScript'
p m%(2)
p n %(2)
EOScript
    assert_equal([
    t(:ident, "p"), sp, t(:ident, "m"), t(:operator, "%"), t(:bracket_open, "("),
    t(:numeric, "2"), t(:bracket_close, ")"), eol,
    t(:ident, "p"), t(:space, " "), t(:ident, "n"), t(:space, " "), t(:string, "%(2)"),
    eol], d)
    end
end
