
# -*- encoding:utf-8 -*-
# rb2html
# Copyright (c) 2001, 2004, 2006 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/
#     mailto:vzw00011@nifty.ne.jp

require 'rb2html/html_formatter'

module Rb2HTML
  class Factory
    TARGETS = {
      ['ruby', '.rb'] => :Ruby2Html,
      ['c', 'c++', '.c', '.cpp', '.cc', '.cxx', '.h', '.m'] => :Cpp2Html,
      ['java', '.java'] => :Java2Html,
      ['python', '.py'] => :Python2Html,
      ['html', 'xhtml', 'xml', '.htm', '.html', '.xml', '.rdf'] => :Html2Html,
      ['css', '.css'] => :CSS2Html
    }
    
    def Factory.has_lang? lang
      TARGETS.each {|names, klass|
        if names.include?(lang)
          return true
        end
      }
      return false
    end

    # 入力
    #     lang 言語名 or 拡張子
    def Factory.get_formatter lang
      TARGETS.each {|pat, klass|
        if pat.include?(lang.downcase)
          require 'rb2html/' + klass.to_s.downcase
          return module_eval(klass.to_s).new(HtmlLayout.new)
        end
      }
      return nil
    end

    def Factory.format lang, source, hash_opt={}
      f = get_formatter lang
      f.format_code source, (hash_opt.has_key?(:lineno) ? hash_opt[:lineno] : 1)
    end
  end
end # of module Rb2HTML
