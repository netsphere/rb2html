
# rb2html
# Copyright (c) 2001, 2004, 2006 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/
#     mailto:vzw00011@nifty.ne.jp

# Rubyスクリプト，Javaソース，C++ソースをHTMLに整形する
# キーワード，コメント，文字列に色を付ける

require 'rb2html/ruby_lexer.rb'
require 'rb2html/source2html.rb'

module Rb2HTML
  class Ruby2Html < Source2Html
    HTML_RULES = [
    [:numeric, {:class => 'num'}],
  [:symbol, {:class => 'sym'}],
  [[:here_mark, :string], {:class => 'str'}],
  [:keyword, {:class => 'keyword'}],
  [:comment, {:class => 'comment'}],
  [:regex, {:class => 'regex'}],
  ]

    def initialize layout
      super layout, RubyLexer.new, HTML_RULES
    end
  end
end # of module Rb2HTML
