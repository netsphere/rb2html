
# -*- encoding:utf-8 -*-
# rb2html
# Copyright (c) 2001, 2004, 2006 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/
#     mailto:vzw00011@nifty.ne.jp

require 'rb2html/python_lexer.rb'
require 'rb2html/source2html.rb'

module Rb2HTML
  HTML_RULES = [
  [:numeric, {:class => 'num'}],
  [:string, {:class => 'str'}],
  [:keyword, {:class => 'keyword'}],
  [:comment, {:class => 'comment'}],
  [:operator, {:class => 'op'}],
  [:assignment, {:class => 'assign'}]
  ]              
              
  class Python2Html < Source2Html
    def initialize layout
      super layout, PythonLexer.new, HTML_RULES
    end
  end
end # of module Rb2HTML
