
# HTML raw parser
# XMLにも対応してるっぽい感じで攻める。

require 'rb2html/pattern_lexer'

module Rb2HTML
  class HTMLRawParser < PatternLexer

    NCName = /[A-Za-z_][A-Za-z0-9._-]*/
    QName = /#{NCName}(:#{NCName})?/
    
    def parse source, &block
      @s = StringScanner.new source
      @parsed = []
      @cur.text = ''
      @state = :TEXT
      
      while !@s.eos?
        case @state
        when :TEXT
          state_text
        when :COMMENT
          state_comment
        when :decl
          state_decl
        when :STAG
          state_stag
        when :ETAG
          state_etag
        else
          raise
        end
      end
      flush_token
      return @parsed
    end
    
    # 地の文
    def state_text
      case
      when @s.scan(/<!--/)
        flush_token Token.new(:comment_start, @s.matched)
        @state = :COMMENT
      when @s.scan(/<\?.*?\?>/)
        flush_token Token.new(:pi, @s.matched)
      when @s.scan(/<%.*?%>/)
        flush_token Token.new(:eruby, @s.matched)
      when @s.scan(/<!\[CDATA\[.*?\]\]>/)
        flush_token Token.new(:cdata, @s.matched)
      when @s.scan(/<!DOCTYPE\s+/)
        flush_token Token.new(:doctype_start, @s.matched)
        @state = :decl
      when @s.scan(/<(#{QName})/)
        flush_token Token.new(:stago, '<')
        flush_token Token.new(:tag_name, @s[1])
        @state = :STAG
      when @s.scan(/<\/(#{QName})(\s*)>/)
        flush_token Token.new(:etago, '</')
        flush_token Token.new(:tag_name, @s[1])
        flush_token Token.new(:space, @s[2]) if @s[2] && @s[2] != ''
        flush_token Token.new(:etagc, '>')
      else
        @cur.symbol = :other
        @cur.text << @s.getch
      end
    end
    
    def state_comment
      case
      when @s.scan(/.*?--/m)
        flush_token Token.new(:comment, @s.matched)
        @state = :decl
      else
        raise
      end
    end
    
    def state_decl
      case
      when @s.scan(/--/)
        flush_token Token.new(:comment_start, @s.matched)
        @state = :COMMENT
      when @s.scan(/>/)
        flush_token Token.new(:decl_end, '>')
        @state = :TEXT
      else
        @cur.symbol = :decl
        @cur.text << @s.getch
      end
    end

    # タグ名の後ろの空白
    def state_stag
      case
      when @s.scan(/(#{QName})(\s*)=/m)
        flush_token Token.new(:att_name, @s[1])
        flush_token Token.new(:space, @s[2]) if @s[2] && @s[2] != ''
        flush_token Token.new(:eq, '=')
      when @s.scan(/(['"]).*?\1/m)
        flush_token Token.new(:att_value, @s.matched)
      when @s.scan(/\w+/)
        flush_token Token.new(:att_value, @s.matched)
      when @s.scan(/>/)
        flush_token Token.new(:stagc, '>')
        @state = :TEXT
      when @s.scan(/\/>/)
        flush_token Token.new(:empty, '/>')
        @state = :TEXT
      when @s.scan(/\s+/)
        flush_token Token.new(:space, @s.matched)
      else
        @cur.symbol = :other
        @cur.text << @s.getch
      end
    end
  end
end

if __FILE__ == $0
  h = Rb2HTML::HTMLRawParser.new
  pa = h.parse <<EOF
<html>
<foo a=b c d="ho'ge">aa</bar><e/><f z::z>< <0> >
EOF
  pa.each {|t|
    puts "#{t.symbol}, '#{t.text}'"
  }
end
