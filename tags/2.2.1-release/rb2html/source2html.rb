
# -*- encoding:utf-8 -*-
# rb2html
# Copyright (c) 2001, 2004, 2006 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/
#     mailto:vzw00011@nifty.ne.jp

require 'rb2html/rb2html-sub'

module Rb2HTML
  
  # 各プログラミング言語を整形するベースクラス
  class Source2Html
    def initialize layout, lexer = nil, html_rules = nil
      @filename = ''
      @fo = layout
      @lexer = lexer
      @html_rules = html_rules
    end
    
    # <head>要素なども出力する。
    def output_file(io_or_str, start_lineno = 1)
      io, @filename = get_io_fn(io_or_str)
      @result = @fo.file_start(@filename)
      @result << @fo.start_formatted()
      parse(io, start_lineno)
      @result << @fo.end_formatted() << @fo.file_end()
      return @result
    end

    # <pre>とその中身だけ出力する。
    # start_lineno  開始行番号。-1だと行番号を出力しない。
    def format_code(io_or_str, start_lineno = -1)
      io, @filename = get_io_fn(io_or_str)

      @result = @fo.start_formatted()
      parse(io, start_lineno)
      @result << @fo.end_formatted()
      return @result
    end

    private
    def get_io_fn(io_or_str)
      if defined?(io_or_str.read)
        io = io_or_str
      else
        require 'stringio'
        io = StringIO.new(io_or_str.chomp)
      end
      return [io, defined?(io.path) ? io.path : nil]
    end

    def out_lno(first = false)
      @result << "\n" if !first
      if @lno >= 0
        @lno += 1
        do_out_lno(@lno)
      end
    end

    def do_out_lno(lno)
      @result << sprintf("%4d| ", lno)
    end

    def parse(io, start_lineno)
      @lno = (start_lineno || 0) - 1
      ary = @lexer.parse(io.read)
      out_lno true
      ary.each {|token|
        ts = token.text.split_str("\n")
        ts.each_with_index {|line, i|
          out_lno if i != 0
          if line != ""
            matched = false
            @html_rules.each {|rule|
              if rule[0].is_a?(Symbol) && token.symbol == rule[0] ||
                rule[0].is_a?(Array) && rule[0].include?(token.symbol)
                tag = '<span'
                rule[1].each {|k, v|
                  tag << " #{k}=\"#{v}\""
                }
                tag << '>'
                @result << tag << Rb2HTML.html_escape(line) << '</span>'
                matched = true
                break
              end
            }
            if !matched
              @result << Rb2HTML.html_escape(line)
            end
          end
        }
      }
    end
  end # of class Source2Html
end # of module Rb2HTML
