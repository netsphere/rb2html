
# -*- encoding:utf-8 -*-
# rb2html
# Copyright (c) 2001, 2004, 2006 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/
#     mailto:vzw00011@nifty.ne.jp

require 'rb2html/html_parse'
require 'rb2html/source2html'

module Rb2HTML
  class Html2Html < Source2Html
    HTML_RULES = [
    [:tag_name, {'style'=>'color:#cc00cc'}],
    [:att_name, {'style'=>'font-weight:bold'}],
    [:att_value, {'style'=>'color:blue'}]
    ]
    
    def initialize layout
      super layout, HTMLRawParser.new, HTML_RULES
    end
  end
end
