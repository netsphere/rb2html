
# -*- encoding:utf-8 -*-
# rb2html
# Copyright (c) 2001, 2004, 2006 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/
#     mailto:vzw00011@nifty.ne.jp

require 'rb2html/css_lexer.rb'
require 'rb2html/source2html.rb'

module Rb2HTML
  HTML_RULES = [
  [:comment, {'class'=>'comment'}],
  [:string, {'class'=>'str'}],
  [:property, {'class'=>'prop'}],
  ]
  
  class CSS2Html < Source2Html
    def initialize layout
      super layout, CSSLexer.new, HTML_RULES
    end
  end
end
