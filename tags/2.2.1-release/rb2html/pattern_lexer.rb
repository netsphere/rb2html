
require 'strscan'

class StringScanner
  def gets(bytes)
    raise TypeError if !bytes.is_a?(Integer)
    r = peek(bytes)
    self.pos += bytes
    return r
  end
end

module Rb2HTML
  class Token
    attr_accessor :symbol, :text
    def initialize(sym, str)
      @symbol = sym
      @text = str
    end
    
    def ==(t)
      return @symbol == t.symbol && @text ==t.text
    end
    
    def inspect
      return "t(:#{@symbol.to_s}, #{@text.inspect})"
    end
  end

  class PatternLexer
    def initialize
      @parsed = []
      @cur = Token.new(nil, '')
    end

    def flush_token token = nil
      if @cur.text != ''
        @parsed << Token.new(@cur.symbol, @cur.text)
        @cur.text = ''
      end
      if token
        @parsed << token
      end
    end
  end
end
