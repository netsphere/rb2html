
# -*- encoding:utf-8 -*-
# rb2html
# Copyright (c) 2001, 2004, 2006 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/
#     mailto:vzw00011@nifty.ne.jp

require 'rb2html/pattern_lexer'

module Rb2HTML
  class PythonLexer < PatternLexer

    PYTHON_PATTERNS = [
    [/([0-9]*\.[0-9]+|[0-9]+\.)([eE][+-]?[0-9]+)?j?/, :numeric],
    [/0[xX][0-9A-Fa-f]+/, :numeric],

    [/[0-9]+[Llj]?/, :numeric],

    [/([ruRU]|[uU][rR])?'''([^']|\\.|\n)*'''/, :string],
    [/([ruRU]|[uU][rR])?"""([^"]|\\.|\n)*"""/, :string],

    # TODO: raw stringの対応
    [/([ruRU]|[uU][rR])?'([^']|\\.)*'/, :string],
    [/([ruRU]|[uU][rR])?"([^"]|\\.)*"/, :string],

    [/[a-zA-Z_][a-zA-Z0-9_]*/, :ident],
    [%r(\+|-|\*|\*\*|/|//|%|<<|>>|&|\||\^|~|<|>|<=|>=|==|!=), :operator],
    
    # Pythonの代入は文
    [%r(=|\+=|-=|\*=|/=|//=|%=|&=|\|=|\^=|>>=|<<=|\*\*=), :assignment],
    
    [/#.*/, :comment],
    [/\n/, :eol],
    [/\s+/, :space],
    ]

    KEYWORDS = 'and       del       for       is        raise    
        assert    elif      from      lambda    return   
        break     else      global    not       try      
        class     except    if        or        while    
        continue  exec      import    pass      yield    
        def       finally   in        print
        as None'.split(/\s+/)

    def parse(source_, &block)
      @scanner = StringScanner.new(source_)
      @parsed = []
      @cur.text = ''
      while !@scanner.eos?
        matched = false
        PYTHON_PATTERNS.each {|pat, sym|
          r = @scanner.scan(pat)
          if r
            #puts "text = #{r}, sym = #{sym}, pat = #{pat}"
            if sym == :ident
              if KEYWORDS.include?(r)
                flush_token Token.new(:keyword, r)
              else
                flush_token Token.new(sym, r)
              end
            else
              flush_token Token.new(sym, r)
            end
            matched = true
            break
          end
        }
        if !matched
          @cur.symbol = :other
          @cur.text << @scanner.getch
        end
      end
      return @parsed
    end
  end
end
