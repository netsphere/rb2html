# -*- coding:utf-8 -*-
# JavaScript字句解析器のテスト

require '../rb2html/javascript_lexer.rb'
require 'test/unit'

def t(sym, text)
  return Rb2HTML::Token.new(sym, text)
end

class JavaScriptLexerTestCase < Test::Unit::TestCase
  SP = t(:space, ' '); DOT = t(:other, '.'); EOL = t(:eol, "\n")

  def setup
    @lex = Rb2HTML::JavaScriptLexer.new
  end

  # 正規表現  
  def test1
    d = @lex.parse '"foo".replace(/(\d)/g, "hoge")'
    assert_equal([
      t(:string, '"foo"'), DOT, t(:ident, 'replace'), t(:bracket_open, '('),
      t(:regex, '/(\d)/g'), t(:delimiter, ','), SP, t(:string, '"hoge"'),
      t(:other, ')')
    ], d)
  end

  # 正規表現と / 演算子の区別
  def test_reg_op
    d = @lex.parse '"abc".match(/a/)'
    assert_equal [
 t(:string, "\"abc\""),
 t(:other, "."),
 t(:ident, "match"),
 t(:bracket_open, "("),
 t(:regex, "/a/"),
 t(:other, ")")
    ], d

    d2 = @lex.parse 'x/=1/2'
    assert_equal [
 t(:ident, "x"),
 t(:operator, "/="),
 t(:numeric, "1"),
 t(:operator, "/"),
 t(:numeric, "2")
    ], d2
  end

  # 将来の予約語？
  def test2
    d = @lex.parse 'super=1'
    assert_equal([
      t(:reserved, 'super'), t(:operator, '='), t(:numeric, '1')
      ], d)
  end

  def test_e4x
    d = @lex.parse 'x=<x><y/></x>;class'
    assert_equal([
      t(:ident, 'x'), t(:operator, '='),
        t(:stago, "<"), t(:tag_name, "x"), t(:stagc, ">"), t(:stago, "<"),
        t(:tag_name, "y"), t(:empty, "/>"), t(:etago, "</"), t(:tag_name, "x"),
        t(:etagc, ">"),
      t(:other, ";"), t(:reserved, "class")
    ], d)
  end
  
  def test_json
    d = @lex.parse 'x={name:"foo"}
do'
    assert_equal([
      t(:ident, "x"), t(:operator, "="), t(:start_json, "{"),
          t(:string, "name"), t(:colon, ":"), t(:string, "\"foo\""),
          t(:end_json, "}"),
      t(:eol, "\n"), t(:keyword, "do")
    ], d)
  end

end
