
require "../rb2html/haskell_lexer.rb"
require 'test/unit'

def t(sym, text)
  return Rb2HTML::Token.new(sym, text)
end

class HaskellLexerTestCase < Test::Unit::TestCase
  def test_lambda
    lex = Rb2HTML::HaskellLexer.new
    d = lex.parse <<'EOF'
main = do
         putStrLn $ show $ map (\x -> x * 2) [1..10]
EOF
    assert_equal [
 t(:variable, "main"),
 t(:space, " "),
 t(:operator, "="),
 t(:space, " "),
 t(:keyword, "do"),
 t(:eol, "\n"),
 t(:space, "         "),
 t(:variable, "putStrLn"),
 t(:space, " "),
 t(:symbol, "$"),
 t(:space, " "),
 t(:variable, "show"),
 t(:space, " "),
 t(:symbol, "$"),
 t(:space, " "),
 t(:variable, "map"),
 t(:space, " "),
 t(:other, "("),
 t(:operator, "\\"),
 t(:variable, "x"),
 t(:space, " "),
 t(:operator, "->"),
 t(:space, " "),
 t(:variable, "x"),
 t(:space, " "),
 t(:symbol, "*"),           # TODO: operatorにすべき
 t(:space, " "),
 t(:numeric, "2"),
 t(:other, ")"),
 t(:space, " "),
 t(:other, "["),
 t(:numeric, "1"),
 t(:operator, ".."),
 t(:numeric, "10"),
 t(:other, "]"),
 t(:eol, "\n")], d
  end
end

