
require "../rb2html/css_lexer.rb"
require 'test/unit'

def t(sym, text)
  return Rb2HTML::Token.new(sym, text)
end

class CssLexerTestCase < Test::Unit::TestCase
  def test_1
    h = Rb2HTML::CSSLexer.new
    pa = h.parse <<EOF
body {
  background:red url("pendant.png");
  background-repeat:repeat-y;
  background-attachment:fixed;
}
EOF
    assert_equal([
 t(:ident, "body"),
 t(:other, " {\n  "),
 t(:property, "background"),
 t(:other, ":"),
 t(:ident, "red"),
 t(:other, " "),
 t(:url_start, "url("),
 t(:string, "\"pendant.png\""),
 t(:other, ");\n  "),
 t(:property, "background-repeat"),
 t(:other, ":"),
 t(:ident, "repeat-y"),
 t(:other, ";\n  "),
 t(:property, "background-attachment"),
 t(:other, ":"),
 t(:ident, "fixed"),
 t(:other, ";\n}\n")], pa)
  end
end

