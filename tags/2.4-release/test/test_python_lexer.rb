
require '../rb2html/python_lexer.rb'
require 'test/unit'

def t(sym, text)
  return Rb2HTML::Token.new(sym, text)
end

class PythonLexerTestCase < Test::Unit::TestCase
  SP = t(:space, ' '); DOT = t(:other, '.'); EOL = t(:eol, "\n")

  def setup
    @lex = Rb2HTML::PythonLexer.new
  end
  
  def test1
    d = @lex.parse <<'EOF'
import os
filename=os.environ.get('PYTHONSTARTUP')
if filename and os.path.isfile(filename):
    execfile(filename)
EOF
    assert_equal([
      t(:keyword, 'import'), SP, t(:ident, 'os'), EOL,
      t(:ident, 'filename'), t(:assignment, '='),
      t(:ident, 'os'), DOT, t(:ident, 'environ'), DOT, t(:ident, 'get'),
      t(:other, '('), t(:string, "'PYTHONSTARTUP'"), t(:other, ')'), EOL,
      t(:keyword, "if"), SP, t(:ident, "filename"), SP, t(:keyword, "and"), SP,
      t(:ident, "os"), DOT, t(:ident, "path"), DOT, t(:ident, "isfile"),
      t(:other, "("), t(:ident, "filename"), t(:other, "):"), EOL,
      t(:space, "    "), t(:ident, "execfile"), t(:other, "("), t(:ident, "filename"),
      t(:other, ")"), EOL], d)
  end
end
