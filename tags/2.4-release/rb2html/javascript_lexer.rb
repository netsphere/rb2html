# -*- coding:utf-8 -*-
# rb2html
# Copyright (c) 2001, 2004, 2006 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/
#     mailto:vzw00011@nifty.ne.jp

if !$__RB2HTML_JS_LEXER__
$__RB2HTML_JS_LEXER__ = true
  
require 'rb2html/pattern_lexer'

module Rb2HTML
  class JavaScriptLexer < PatternLexer
    BASE_PATTERNS = [
      # float
      [/([0-9]+\.[0-9]*|\.[0-9]+)([eE][+-]?[0-9]+)?/, :numeric], # 0. or .0
      [/[0-9]+[eE][+-]?[0-9]+/, :numeric],                       # 1e1

      # integer
      [/0[xX][0-9a-fA-F]+/, :numeric],
      [/[0-9]+/, :numeric],

      # comment
      [%r(/\*(.|\n)*?\*/), :comment], # ネストなし
      [%r(//.*), :comment],          # 行末まで

      # string  改行は含められない
      [/"(\\.|[^"])*"/, :string],  #"
      [/'(\\.|[^'])*'/, :string],  #'
    ]

    JS_PATTERNS = BASE_PATTERNS + [
      # ident   正しくはUnicode文字の多くが使える
      [/[a-zA-Z_][a-zA-Z_0-9]*/, :ident],

      # json
      [/\{/, :start_json, {:only_after => [:operator, :delimiter, :bracket_open]}],

      # e4x
      [/</, :start_e4x, {:only_after => :operator}],

      # operator
      [%r(\+=|-=|\*=|&=|\|=|\^=|%=|<<=|>>=|>>>=), :operator],
      [/===|!==|==|<=|>=|!=|&&|\|\|/, :operator],
      [/\+\+|--/, :op_unary],
      [/<<|>>|>>>/, :operator],
      [%r(=|>|<|!|~|\?|:|\+|-|\*|&|\||\^|%), :operator],
      [%r(/=|/), :operator,
                     {:only_after => [:numeric, :ident, :string, :regex]}],

      # /* or // はコメントとしてよい。空の正規表現は/(?:)/と書く。
      [%r(/(\\.|[^/])+/[A-Za-z]?), :regex],
      [/\//, :operator], 

      [/,/, :delimiter],
      [/[(\[]/, :bracket_open],
      [/\r?\n|\r/, :space, {:only_after => :operator}],
      [/\r?\n|\r/, :eol],
      [/[ \t]+/, :space],
    ]

    # TODO: もそっとまともに。
    # http://www.ietf.org/rfc/rfc4627.txt
    SJSON_PATTERNS = BASE_PATTERNS + [
      # ident   jsonのなかでは文字列。
      [/[a-zA-Z_][a-zA-Z_0-9]*/, :string],

      [/:/, :colon],
      [/\s+/, :space],
      [/\}/, :end_json],
    ]

    KEYWORDS = %w(break    else       new    var
                case     finally    return void
                catch    for        switch while
                continue function   this   with
                default  if         throw
                delete   in         try
                do       instanceof typeof
                null true false)

    # Javaのキーワード？
    RESERVED = %w(abstract enum       int       short
                boolean  export     interface static
                byte     extends    long      super
                char     final      native    synchronized
                class    float      package   throws
                const    goto       private   transient
                debugger implements protected volatile
                double   import     public)

    def state_0
      matched = match(JS_PATTERNS) {|sym, r|
        #puts "text = #{r}, sym = #{sym}, pat = #{pat}"     # DEBUG
        if sym == :start_json
          flush_token Token.new(:start_json, r)
          @state = :json
        elsif sym == :start_e4x
          @scanner.unscan
          @state = :e4x
        elsif sym == :ident
          if KEYWORDS.include?(r)
            flush_token Token.new(:keyword, r)
          elsif RESERVED.include?(r)
            flush_token Token.new(:reserved, r)
          else
            flush_token Token.new(sym, r)
          end
        else
          flush_token Token.new(sym, r)
        end
      }

      if !matched
        @last_kind = @cur.symbol = :other
        @cur.text << @scanner.getch
      end
    end

    def state_json
      matched = match(SJSON_PATTERNS) {|sym, r|
        if sym == :end_json
          flush_token Token.new(sym ,r)
          @state = 0
        else
          flush_token Token.new(sym, r)
        end
      }

      if !matched
        @last_kind = @cur.symbol = :other
        @cur.text << @scanner.getch
      end
    end

    def state_e4x
      require 'rb2html/html_parse'
      ary, rest = HTMLRawParser.new.parse @scanner.rest, true
      ary.each {|e|
        flush_token e
      }
      @scanner.string = rest
      @state = 0
    end

    def parse(source_, &block)
      super(source_)
      @state = 0
      @last_kind = :begin_of_source
      while !@scanner.eos?
        case @state
        when 0
          state_0
        when :json
          state_json
        when :e4x
          state_e4x
        else
          raise "internal error"
        end
      end
      flush_token
      return @parsed
    end

  end # of class JavaScriptLexer
end

end

