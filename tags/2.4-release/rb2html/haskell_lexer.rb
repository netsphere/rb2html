# -*- coding:utf-8 -*-
# rb2html
# Copyright (c) 2001, 2004, 2006 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/
#     mailto:vzw00011@nifty.ne.jp

require 'rb2html/pattern_lexer'

module Rb2HTML
  class HaskellLexer < PatternLexer
    SYMBOL = %r([!#\$\%&*+./<=>?@\\^|\-~])
    NOT_SYM = %r([^!#\$\%&*+./<=>?@\\^|~])  # -を除く

    S0_PATTERNS = [
      # float
      [/[0-9]+\.[0-9]+([eE][+-]?[0-9]+)?/, :numeric], # 0.0 or 1.0e-1
      [/[0-9]+[eE][+-]?[0-9]+/, :numeric],            # 1e1

      # integer
      [/0[xX][0-9a-fA-F]+/, :numeric],
      [/0[oO][0-7]+/, :numeric],
      [/[0-9]+/, :numeric],

      # ident
      [/[a-z_][a-zA-Z0-9'_]*/, :variable],   #'
      [/[A-Z][a-zA-Z0-9'_]*/, :ctor],        #' constructor

      # symbol or operator
      [/--+($|#{NOT_SYM}).*/, :comment],
      [/#{SYMBOL}(#{SYMBOL}|\:)*/, :symbol],      
      [/:(#{SYMBOL}|:)*/, :symbol], 

      # comment
      [/\{-/, :comment_begin], # haskellのコメントはネストする

      # single character
      [/'(\\.|[^'])*'/, :string],  #'

      # string
      [/"(\\.|[^"])*"/, :string],  #"

      [/\r?\n|\r/, :eol],
      [/\s+/, :space],
    ]

    SCOMMENT_PATTERNS = [
      [/\{-/, :comment_begin],
      [/-\}/, :comment_end],
    ]
  
    KEYWORDS = %w(case class  data    default deriving do     else
                if   import in      infix   infixl   infixr instance
                let  module newtype of      then     type   where    
                _)

    OPS = %w(.. : :: = \\ | <- -> @ ~ =>)

    def state_0
      match(S0_PATTERNS) {|sym, r|
        case sym
        when :variable
          if KEYWORDS.include?(r)
            flush_token Token.new(:keyword, r)
          else
            flush_token Token.new(sym, r)
          end
        when :comment_begin
          flush_token
          @cur.symbol = :comment
          @cur.text = r
          @state = :comment
          @comment_depth = 1
        when :symbol
          if OPS.include?(r)
            flush_token Token.new(:operator, r)
          else
            flush_token Token.new(sym, r)
          end
        else
          flush_token Token.new(sym, r)
        end
        return
      }
      @cur.symbol = :other
      @cur.text << @scanner.getch
    end

    def state_comment
      match(SCOMMENT_PATTERNS) {|sym, r|
        @cur.text << r
        case sym
        when :comment_begin
          @comment_depth += 1
        when :comment_end
          @comment_depth -= 1
          if @comment_depth == 0
            @state = 0 
            flush_token
          end
        end
        return
      }
      @cur.text << @scanner.getch
    end

    # @override
    def parse source_
      super(source_)
      @state = 0
      while !@scanner.eos?
        case @state
        when 0
          state_0
        when :comment
          state_comment
        else
          raise "internal error"
        end
      end
      flush_token
      return @parsed
    end
  end 
end

