
# -*- encoding:utf-8 -*-
# rb2html
# Copyright (c) 2001, 2004, 2006 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/
#     mailto:vzw00011@nifty.ne.jp

module Rb2HTML

  # HTMLに整形するためのフォーマッタ
  # Source2Htmlおよびそのサブクラスから呼び出される
  class HtmlLayout
    def file_start(filename)
      return <<EOF
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
            "http://www.w3.org/TR/html4/loose.dtd">
<html lang="ja">
<head>
  <title>source of #{filename}</title>
  <style type="text/css">
div.source {
  background-color:#e8e8e8;  /* IE対策. 行番号の背景色 */
}
div.source ol {
  list-style-position: outside;
  line-height:1.0;
  font-family:monospace;
  font-size:10pt;
}
div.source ol.nolineno {
  list-style-type:none;
}
div.source li {
  margin-left: 10px;  /* 行番号 */
  padding-left: 8px;  /* 本文 */
  background-color:#f4f4f4;
  border-left: solid 3px #4DB56A;
  white-space:pre;
}

.source .literal { color:#660066; }
.source .comment { color:green; }
.source .keyword { color:blue; }
.source .preprocessor { color:purple; }
</style>
</head>
<body>
<div style="color:blue;margin-top:1em;font-family:sans-serif">#{filename}</div>
EOF
  end

  def file_end()
    return "</body></html>\n"
  end

  # 
  def start_formatted(start_lno)
    return <<EOF
<div class="source"><ol #{start_lno && start_lno >= 1 ?
          'start="' + start_lno.to_s + '"' : 'class="nolineno"' }>
EOF
  end

  def end_formatted()
    '</ol></div>'
  end

  def begin_literal; '<span class="literal">' end
  def end_literal; '</span>' end

  def begin_comment; '<span class="comment">' end
  def end_comment; '</span>' end

  def begin_keyword; '<span class="keyword">' end
  def end_keyword; '</span>' end
      
  # for C/C++
  def begin_preprocessor
    '<span class="preprocessor">'
  end
  def end_preprocessor
    '</span>'
  end
end

end # of Rb2HTML
