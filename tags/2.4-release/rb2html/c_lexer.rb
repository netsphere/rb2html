# -*- encoding:utf-8 -*-

# rb2html
# Copyright (c) 2001, 2004, 2006 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/
#     mailto:vzw00011@nifty.ne.jp

require 'rb2html/pattern_lexer'

module Rb2HTML
  # C/C++/Objective-C
  class CLexer < PatternLexer
    KEYWORDS = %w(restrict _Bool _Complex _Imaginary export
      asm          do             inline             short         typeid       
      auto         double         int                signed        typename     
      bool         dynamic_cast   long               sizeof        union        
      break        else           mutable            static        unsigned     
      case         enum           namespace          static_cast   using        
      catch        explicit                          struct        virtual      
      char         extern         operator           switch        void         
      class        false          private            template      volatile     
      const        float          protected          this          wchar_t      
      const_cast   for            public             throw         while        
      continue     friend         register           true                       
      default      goto           reinterpret_cast   try                        
      if           return         typedef    id)

    PP_KEYWORDS = %w(if ifdef ifndef elif else endif include define 
                     undef line error pragma)

    OP_KEYWORDS = %w(new delete and      and_eq   bitand   bitor   compl    not 
      not_eq   or       or_eq    xor     xor_eq)

    # Objective-C
    OBJC_KW = %w(interface implementation protocol end
      private protected package public try          throw catch finally
      class   selector  encode  defs   synchronized)

    S0_PATTERNS = [
      # float
      [/([0-9]+\.[0-9]*|\.[0-9]+)([eE][+-]?[0-9]+)?[fFlL]?/, :numeric], # 0. or .0
      [/[0-9]+[eE][+-]?[0-9]+[fFlL]?/, :numeric],                       # 1e1

      # integer
      # LL = [C99] long long int
      [/0[xX][0-9a-fA-F]+[uU]?(LL|ll|[lL])?/, :numeric],
      [/0[xX][0-9a-fA-F]+(LL|ll|[lL])[uU]?/, :numeric],
      [/[0-9]+[uU]?(LL|ll|[lL])?/, :numeric],
      [/[0-9]+(LL|ll|[lL])[uU]?/, :numeric],

      # character literals ... 複数文字を書いてもよい。=> type int
      [/L?'(\\.|[^'])*'/, :string],  #'
      # string
      [/L?"(\\.|[^"])*"/, :string],  #"

      # ident
      [/[a-zA-Z_][a-zA-Z_0-9]*/, :ident],
      [/@[a-z][a-z]*/, :annotation],

      # comment
      [%r(/\*(.|\n)*?\*/), :comment], # ネストなし
      [%r(//.*), :comment],          # 行末まで

      # operator
      [%r(\+=|-=|\*=|/=|&=|\|=|\^=|%=|<<=|>>=), :operator],
      [/==|<=|>=|!=|&&|\|\||\+\+|--/, :operator],
      [/<<|>>/, :operator],
      [%r(=|>|<|!|~|\?|:|\+|-|\*|/|&|\||\^|%), :operator],

      [/\r?\n/, :eol],  # プリプロセッサ判定のために改行を切り出す
      [/[ \t]+/, :space],
    ]

    # @override
    def parse source
      super(source)
      while !@scanner.eos?
        state_0
      end
      flush_token
      return @parsed
    end

    private

    # 地の文
    def state_0
      if @scanner.beginning_of_line?
        if (r = @scanner.scan(/([ \t]*)(\#[ \t]*)([a-z]+)/))
          # preprocessing directives
          if @scanner[1] && @scanner[1] != ""
            flush_token Token.new(:space, @scanner[1])
          end
          if PP_KEYWORDS.include?(@scanner[3])
            flush_token Token.new(:cpp, @scanner[2] + @scanner[3])
          else
            flush_token Token.new(:cpp, @scanner[2])
            flush_token Token.new(:other, @scanner[3])
          end
          return
        end
      end

      match(S0_PATTERNS) {|sym, r|
        case sym
        when :ident
          if KEYWORDS.include?(r)
            flush_token Token.new(:keyword, r)
          elsif OP_KEYWORDS.include?(r)
            flush_token Token.new(:kw_op, r)
          else
            flush_token Token.new(sym, r)
          end
        when :annotation
          if OBJC_KW.include?(r[1..-1])
            flush_token Token.new(:keyword, r)
          else
            flush_token Token.new(:ident, r)
          end
        else
          flush_token Token.new(sym, r)
        end
        return
      }
      @cur.symbol = :other
      @cur.text << @scanner.getch
    end

  end # of class CLexer
end # of module Rb2HTML

