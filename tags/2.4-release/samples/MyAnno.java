// -*- coding:utf-8 -*-
// カスタムアノテーションのサンプル

@interface _inP {}

public class MyAnno {
    @_inP
    public void calc() {}
}
