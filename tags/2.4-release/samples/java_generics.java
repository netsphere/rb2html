
import java.util.LinkedList;
import java.util.List;

class GenericsSample {
  public static void main(String[] args) {
    List<LinkedList<String>> list = new LinkedList<LinkedList<String>>();
    for (int i = 0; i < args.length; i++) {
      LinkedList<String> l2 = new LinkedList<String>();
      l2.add(args[i]);
      list.add(l2);
    }
    for (int i = 0; i < list.size(); i++) {
      String text = list.get(i).get(0);
      System.out.println(text);
    }
  }
}

