-- ふつうのHaskell p.182
{-コメントは{-ネストする。-}まだコメント-}
(/$$/)::Float -> Float -> Float
(/$$/) x y = x + y

main = do
  print $ 5.3 /$$/ 4.5
