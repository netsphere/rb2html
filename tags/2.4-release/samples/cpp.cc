/*
  C++ソースのサンプル
*/

#include <stdio.h> // printf

class Hoge {
public:
    virtual void p() = 0;
};

class HogeHoge: public Hoge {
public:
    virtual void p() { printf("hogehoge\n"); }
};

int main(int argc, char* argv[])
{
    Hoge* h = new HogeHoge();
    h->p();
    delete h;
    return 0;
}
