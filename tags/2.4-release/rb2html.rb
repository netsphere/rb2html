#!/usr/bin/ruby
# -*- encoding:utf-8 -*-
# rb2html
# Copyright (c) 2001, 2004, 2006 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/
#     mailto:vzw00011@nifty.ne.jp

require 'rb2html/factory'
require 'optparse'

ProgramConfig = {}
OptionParser.new {|opt|
  opt.on('-x <language>', %w[ruby python css html xml c c++ java]) {|v| ProgramConfig[:x] = v}
  opt.parse! ARGV
}

if ARGV.empty?
  if ProgramConfig[:x]
    lang = ProgramConfig[:x]
  else
    # rb2html, c2html -> 'rb', 'c'
    lang = '.' + File.basename($0).split('2', 2)[0]
  end
  f = Rb2HTML::Factory.get_formatter lang
  raise "unknown language: '#{lang}'" if !f
  print f.output_file($stdin)
else
  ARGV.each {|fname|
    if ProgramConfig[:x]
      lang = ProgramConfig[:x]
    else
      lang = '.' + File.basename(fname).split('.', 2)[1]
      if !Rb2HTML::Factory.has_lang?(lang)
        lang = '.' + File.basename($0).split('2', 2)[0]
      end
    end
    f = Rb2HTML::Factory.get_formatter lang
    raise "unknown language: '#{lang}'" if !f
    File.open(fname) {|io| print f.output_file(io) }
  }
end
