# 日記ファイル中に埋め込まれたRubyスクリプトを整形する

require 'rb2html.rb'

def parse(fp)
  state = 1
  while line = fp.gets
    line.gsub! /[\r\n]/, ''
    case state
    when 1
      if line =~ '<d:source>'
        print $`, "\n"
        print "<pre>"
        parser = Ruby2Html.new
        state = 2
      else
        print line
      end
    when 2
      line = $` + $' if line =~ '<!\[CDATA\[' || line =~ ']]>'
      if line =~ '</d:source>'
        parser.parse_line($`)
        print "</pre>\n"
        print $'
        state = 1
      else
        parser.parse_line(line)
      end
    end
    print "\n"
  end
end

if __FILE__ == $0
  if ARGV[0]
    fp = File.open(ARGV[0])
    parse(fp)
    fp.close
  else
    parse($stdin)
  end
end
