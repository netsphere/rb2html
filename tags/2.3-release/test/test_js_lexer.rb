# -*- coding:utf-8 -*-
# JavaScript字句解析器のテスト

require '../rb2html/javascript_lexer.rb'
require 'test/unit'

def t(sym, text)
  return Rb2HTML::Token.new(sym, text)
end

class JavaScriptLexerTestCase < Test::Unit::TestCase
  SP = t(:space, ' '); DOT = t(:other, '.'); EOL = t(:eol, "\n")

  def setup
    @lex = Rb2HTML::JavaScriptLexer.new
  end
  
  def test1
    d = @lex.parse '"foo".replace(/(\d)/g, "hoge")'
    assert_equal([
      t(:string, '"foo"'), DOT, t(:ident, 'replace'), t(:other, '('),
      t(:regex, '/(\d)/g'), t(:other, ','), SP, t(:string, '"hoge"')
      ], d)
  end

  # 将来の予約語？
  def test2
    d = @lex.parse 'super=1'
    assert_equal([
      t(:reserved, 'super'), t(:operator, '='), t(:numeric, '1')
      ], d)
  end
end
