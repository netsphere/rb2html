
require "../rb2html/html_parse.rb"
require 'test/unit'

def t(sym, text)
  return Rb2HTML::Token.new(sym, text)
end

class HTMLTestCase < Test::Unit::TestCase
  def setup
    @lex = Rb2HTML::HTMLRawParser.new
  end

  def test_erb
    sp = t(:space, ' '); eq = t(:eq, '=')
    d = @lex.parse('<div id=1><% class Foo; end %></div>')
    assert_equal([
      t(:stago, '<'), t(:tag_name, 'div'), sp, t(:att_name, 'id'), eq,
      t(:att_value, '1'), t(:stagc, '>'), t(:eruby, '<% '),
      # ruby script
      t(:class, 'class'), sp, t(:class_name, 'Foo'), t(:delimiter, ';'), sp,
      t(:keyword, 'end'), sp,
      t(:eruby, '%>'),
      t(:etago, '</'),
      t(:tag_name, 'div'), t(:etagc, '>')], d)
  end
end
