# -*- coding:utf-8 -*-

require '../rb2html/java_lexer.rb'
require 'test/unit'

def t(sym, text)
  return Rb2HTML::Token.new(sym, text)
end

class JavaLexerTestCase < Test::Unit::TestCase
  SP = t(:space, ' '); EOL = t(:eol, "\n")

  def setup
    @lex = Rb2HTML::JavaLexer.new
  end
  
  def test1
    d = @lex.parse %q(3.14f 0777L null 0xDadaCafe '\\')
    assert_equal([
      t(:numeric, '3.14f'), SP, 
      t(:numeric, '0777L'), SP,
      t(:keyword, 'null'), SP,
      t(:numeric, '0xDadaCafe'), SP,
      t(:string, %q('\\'))
    ], d)
  end

  # コメント
  def test_comment
    d = @lex.parse %q(3/2/* this comment /* // /** ends here: */)
    assert_equal([
      t(:numeric, '3'), t(:operator, '/'), t(:numeric, '2'),
      t(:comment, %q(/* this comment /* // /** ends here: */))], d)
  end
end
