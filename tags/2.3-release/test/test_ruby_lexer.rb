# -*- coding:utf-8 -*-

require "../rb2html/ruby_lexer.rb"
require 'test/unit'

def t(sym, text)
  return Rb2HTML::Token.new(sym, text)
end

class RubyLexerTestCase < Test::Unit::TestCase
  SP = t(:space, ' '); EOL = t(:eol, "\n")
  COMMA = t(:delimiter, ','); EQ = t(:operator, '=')

  def setup
    @lex = Rb2HTML::RubyLexer.new
  end
  
  def test0
    # 数値リテラル、文字列、正規表現
    d = @lex.parse <<'EOScript'
p 1.1e-2, 0x0aaff/2,'fo\'o\\'
ABC=/abc/m
EOScript
    
    assert_equal([
    t(:ident, 'p'), SP, t(:numeric, '1.1e-2'), COMMA, SP,
    t(:numeric, '0x0aaff'), t(:operator, '/'), t(:numeric, '2'), COMMA,
    t(:string, "'fo\\'o\\\\'"), EOL,
    t(:constant, "ABC"), EQ, t(:regex, '/abc/m'), EOL],
    d)
    
    # キーワード、メソッド、定数、インスタンス変数、クラス変数、=begin...=end
    d = @lex.parse <<'EOScript'
def foo end;p foo.is_a?(Class)
@b=@@c=Const="hog
e"  # comment
=begin foobar
here is comment
=end
p defined?(foo.def),$1
EOScript
    
    assert_equal([
    t(:keyword, 'def'), SP, t(:ident, 'foo'), SP, t(:keyword, 'end'), t(:delimiter, ';'),
    t(:ident, 'p'), SP, t(:ident, 'foo'), t(:dot, '.'), t(:method, 'is_a?'),
    t(:bracket_open, '('), t(:constant, 'Class'), t(:bracket_close, ')'), EOL,
    t(:instance_variable, "@b"), EQ, t(:class_variable, "@@c"), EQ, t(:constant, "Const"),
    EQ, t(:string, "\"hog\ne\""), t(:space, "  "), t(:comment, "# comment"), EOL,
    t(:comment, "=begin foobar\nhere is comment\n=end\n"),
    t(:ident, "p"), SP, t(:keyword, "defined?"), t(:bracket_open, "("), t(:ident, "foo"),
    t(:dot, "."), t(:method, "def"), t(:bracket_close, ")"), COMMA, t(:global_variable, "$1"),
    EOL], d)

    # ヒアドキュメント、シンボル、%表記
    d = @lex.parse <<'EOScript'
p <<EOF,<<'HOGE',:hoge=,:[]
text
EOF
hogehoge
HOGE
a=[?c]+%w(foo bar)
EOScript
    assert_equal([
    t(:ident, 'p'), SP, t(:here_mark, '<<EOF'), COMMA, t(:here_mark, "<<'HOGE'"), COMMA,
    t(:symbol, ':hoge='), COMMA, t(:symbol, ':[]'), EOL,
    t(:string, "text\nEOF"), EOL, t(:string, "hogehoge\nHOGE"), EOL,
    t(:ident, 'a'), EQ, t(:bracket_open, '['), t(:numeric, '?c'), t(:bracket_close, ']'),
    t(:operator, '+'), t(:array, '%w(foo bar)'), EOL], d)
  end
  
  # ヒアドキュメント <<-形式
  def test1
    d = @lex.parse <<'EOScript'
p <<-EOS
  text.
  EOS
EOScript
    assert_equal([
      t(:ident, 'p'), SP, t(:here_mark, '<<-EOS'), EOL, 
      t(:string, "  text.\n  EOS"), EOL], d)
  end
=begin
EOS
=end
  
  # %形式
  def test2
    d = @lex.parse('p %Q(foo),%q[bar],%r!hoge!,%s<funi>')
    assert_equal([
    t(:ident, 'p'), SP, t(:string, '%Q(foo)'), COMMA,
    t(:string, '%q[bar]'), COMMA,
    t(:regex, '%r!hoge!'), COMMA,
    t(:symbol, '%s<funi>')], d)
  end

  # 文字列、正規表現の式展開
  def test3
    d = @lex.parse File.read('data/ruby_3.rb')
    assert_equal([
    t(:ident, 's'), EQ, t(:string, '"abc #{t = ""; (1..3).each {|i| t << i}; t} bar"'),
    EOL,
    t(:ident, 't'), EQ, t(:regex, '/foo#{u = /abc/; (1..3).each {"a"}}bar/'), EOL], d)
  end

  # '/'  正規表現リテラル or 演算子
  def test4
    d = @lex.parse <<'EOScript'
p m/2#/
p n /2#/
EOScript
    
    mp = t(:ident, 'p') 
    assert_equal([
    mp, SP, t(:ident, 'm'), t(:operator, '/'), t(:numeric, '2'),
    t(:comment, '#/'), EOL,
    mp, SP, t(:ident, 'n'), SP, t(:regex, '/2#/'), EOL], d)
  end

  # '%'  演算子 or %記法
  def test5
    d = @lex.parse <<'EOScript'
p m%(2)
p n %(2)
EOScript
    assert_equal([
    t(:ident, "p"), SP, t(:ident, "m"), t(:operator, "%"), t(:bracket_open, "("),
    t(:numeric, "2"), t(:bracket_close, ")"), EOL,
    t(:ident, "p"), SP, t(:ident, "n"), SP, t(:string, "%(2)"),
    EOL], d)
  end

  # 特異クラスなど
  def test_class
    d = @lex.parse %q(class<<obj)
    assert_equal([
      t(:class, 'class'), t(:operator, '<<'), t(:ident, 'obj')], d)

    d = @lex.parse %q(class Foo::Bar)
    assert_equal([
      t(:class, 'class'), SP, t(:class_name, 'Foo::Bar')], d)
  end

  # シンボル=の=はシンボルの一部か
  def test_sym_eq
    d = @lex.parse "a=:hoge=;b={:hoge= =>1,:foo=>2}"
    assert_equal([
      t(:ident, "a"), t(:operator, "="), t(:symbol, ":hoge="), t(:delimiter, ';'),
      t(:ident, 'b'), t(:operator, '='), t(:bracket_open, '{'), 
      t(:symbol, ':hoge='), SP, t(:delimiter, '=>'), t(:numeric, '1'), COMMA, 
      t(:symbol, ':foo'), t(:delimiter, '=>'), t(:numeric, '2'),
      t(:bracket_close, '}')], d)
  end
end
