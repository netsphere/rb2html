# -*- coding:utf-8 -*-

require 'strscan'

class StringScanner
  def gets(bytes)
    raise TypeError if !bytes.is_a?(Integer)
    r = peek(bytes)
    self.pos += bytes
    return r
  end
end

module Rb2HTML
  class Token
    attr_accessor :symbol, :text
    def initialize(sym, str)
      @symbol = sym
      @text = str
    end
    
    def ==(t)
      return @symbol == t.symbol && @text ==t.text
    end
    
    def inspect
      return "t(:#{@symbol.to_s}, #{@text.inspect})"
    end
  end

  class PatternLexer
    def initialize
      @parsed = []
      @cur = Token.new(nil, '')
    end

    # 必要に応じてサブクラスで上書きする
    def add_token token
      @parsed << token
    end

    def flush_token token = nil
      if @cur.text != ''
        add_token(Token.new(@cur.symbol, @cur.text))
        @cur.text = ''
      end
      add_token(token) if token
    end
  end
end
