# -*- coding:utf-8 -*-

# rb2html
# Copyright (c) 2001, 2004, 2006 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/
#     mailto:vzw00011@nifty.ne.jp

require 'rb2html/source2html'

module Rb2HTML

class Java2HtmlOld < Source2Html
  def initialize(formatter)
    super(formatter)
    
    @que = ''
    @line = ''
    @state = 1

    @keywords = Hash.new
    # Keywords, The Null Literal, Boolean Literals
      'abstract    default    if            private      this
      boolean     do         implements    protected    throw
      break       double     import        public       throws
      byte        else       instanceof    return       transient
      case        extends    int           short        try
      catch       final      interface     static       void
      char        finally    long          strictfp     volatile
      class       float      native        super        while
      const       for        new           switch
      continue    goto       package       synchronized
      null true false'.split(/\s+/).each {|k| @keywords[k] = 1 }

    @strend = '"'
  end
          
  # 地の文の出力, キーワードの色付け
  def que_out()
    while @que =~ /[\@a-zA-Z_][a-zA-Z0-9_]*[?!]?/
      @que = $'
      if @keywords[$&] && $`[-1] != ?.
        @result << Rb2HTML.html_escape($`) << @fo.begin_keyword << $& << @fo.end_keyword
      else
        @result << Rb2HTML.html_escape($`) << $&
      end
    end
    @result << Rb2HTML.html_escape(@que)
    @que = ''
  end
          
  def parse(fp, start_lineno)
    lno = start_lineno
    while line = fp.gets
      if lno >= 0
        do_out_lno(lno)
        lno += 1
      end
      line.gsub! /[\r\n]/, ''
      parse_line(line)
      @result << "\n"
    end
  end # of parse()

  def state1(ch)
    # 地の文
    case ch
    when '"'
      que_out()
      @result << @fo.begin_literal << "&quot;"
      @strend = '"'
      @state = 2
      return
    when '\''
      que_out()
      @result << @fo.begin_literal << "'"
      @strend = '\''
      @state = 2
      return
    when '/'
      if @line[0, 1] == '*'
        que_out()
        @result << @fo.begin_comment << "/*"
        @line.shift
        @state = 4 # 伝統的コメント
        return
      elsif @line[0, 1] == '/'
        que_out()
        @result << @fo.begin_comment << "/" << Rb2HTML.html_escape(@line) << @fo.end_comment
        @line = ''
        return
      end
    end
    @que << ch
  end # state1()
  
  def state2(ch)
    # "..." String Literals
    # 'a' Character Literals
    case ch
    when @strend
      @result << Rb2HTML.html_escape(@que + ch) << @fo.end_literal
      @que = ''
      @state = 1
      return
    when '\\'
      if @line != ''
        @que << ch << @line.shift
        return
      end
    end
    @que << ch
  end

  def state4(ch)
    # コメント
    case ch
    when '*'
      if @line[0, 1] == '/'
        @result << Rb2HTML.html_escape(@que) << "*/" << @fo.end_comment
        @line.shift
        @que = ''
        @state = 1
        return
      end
    end
    @que << ch
  end

  def parse_line(s)
    @line = s
    @result << @fo.begin_comment if @state == 4 # /* ... */

    while @line != ''
      ch = @line.shift
      case @state
      when 1
        state1(ch)
      when 2
        state2(ch)
      when 4
        state4(ch)
      else
        print "Internal error.\n"
        exit 1
      end
    end

    if @state == 1
      que_out()
    else
      @result << Rb2HTML.html_escape(@que)
      @que = ''
    end
    
    @result << @fo.end_comment if @state == 4
  end #parse_line()
end

end # of module Rb2HTML
