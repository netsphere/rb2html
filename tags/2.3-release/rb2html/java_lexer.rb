# -*- coding:utf-8 -*-
# rb2html
# Copyright (c) 2001, 2004, 2006 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/
#     mailto:vzw00011@nifty.ne.jp

require 'rb2html/pattern_lexer'

module Rb2HTML
  class JavaLexer < PatternLexer
    JAVA_PATTERNS = [
      # float
      [/([0-9]+\.[0-9]*|\.[0-9]+)([eE][+-]?[0-9]+)?[fFdD]?/, :numeric], # 0. or .0
      [/[0-9]+[eE][+-]?[0-9]+[fFdD]?/, :numeric],                       # 1e1
      [/[0-9]+([eE][+-]?[0-9]+)?[fFdD]/, :numeric],                     # 1f

      # integer
      [/0[xX][0-9a-fA-F]+[lL]?/, :numeric],
      [/[0-9]+[lL]?/, :numeric],

      # ident
      [/[a-zA-Z_][a-zA-Z_0-9]*/, :ident],

      # アノテーション
      [/@interface/, :keyword],
      [/@[a-zA-Z_][a-zA-Z_0-9]*/, :annotation],

      # comment
      [%r(/\*(.|\n)*?\*/), :comment], # ネストなし
      [%r(//.*), :comment],          # 行末まで

      # operator
      [%r(\+=|-=|\*=|/=|&=|\|=|\^=|%=|<<=|>>=|>>>=), :operator],
      [/==|<=|>=|!=|&&|\|\||\+\+|--/, :operator],
      [/<<|>>|>>>/, :operator],
      [%r(=|>|<|!|~|\?|:|\+|-|\*|/|&|\||\^|%), :operator],

      # single character やや手抜き。\uFFFFなどがある。
      [/'([^']|\\.)*'/, :string],  #'

      # string
      [/"([^"]|\\.)*"/, :string],  #"

      [/\r?\n|\r/, :eol],
      [/\s+/, :space],
    ]

    # assert, enumが追加になっている。
    KEYWORDS = 'abstract    continue    for           new          switch
        assert      default     if            package      synchronized
        boolean     do          goto          private      this
        break       double      implements    protected    throw
        byte        else        import        public       throws
        case        enum        instanceof    return       transient
        catch       extends     int           short        try
        char        final       interface     static       void 
        class       finally     long          strictfp     volatile
        const       float       native        super        while
        null true false'.split(/\s+/)

    # pythonとまるっきり一緒。
    def parse(source_, &block)
      @scanner = StringScanner.new(source_)
      @parsed = []
      @cur.text = ''
      while !@scanner.eos?
        matched = false
        JAVA_PATTERNS.each {|pat, sym|
          r = @scanner.scan(pat)
          next if !r

          #puts "text = #{r}, sym = #{sym}, pat = #{pat}"
          if sym == :ident
            if KEYWORDS.include?(r)
              flush_token Token.new(:keyword, r)
            else
              flush_token Token.new(sym, r)
            end
          else
            flush_token Token.new(sym, r)
          end
          matched = true
          break
        }
        if !matched
          @cur.symbol = :other
          @cur.text << @scanner.getch
        end
      end
      return @parsed
    end

  end # of JavaLexer class
end

