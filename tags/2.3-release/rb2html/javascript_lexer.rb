# -*- coding:utf-8 -*-
# rb2html
# Copyright (c) 2001, 2004, 2006 HORIKAWA Hisashi. All rights reserved.
#     http://www.nslabs.jp/
#     mailto:vzw00011@nifty.ne.jp

require 'rb2html/pattern_lexer'

module Rb2HTML
  class JavaScriptLexer < PatternLexer
    JS_PATTERNS = [
      # float
      [/([0-9]+\.[0-9]*|\.[0-9]+)([eE][+-]?[0-9]+)?/, :numeric], # 0. or .0
      [/[0-9]+[eE][+-]?[0-9]+/, :numeric],                       # 1e1

      # integer
      [/0[xX][0-9a-fA-F]+/, :numeric],
      [/[0-9]+/, :numeric],

      # ident   正しくはUnicode文字の多くが使える
      [/[a-zA-Z_][a-zA-Z_0-9]*/, :ident],

      # comment
      [%r(/\*(.|\n)*?\*/), :comment], # ネストなし
      [%r(//.*), :comment],          # 行末まで

      # operator
      [%r(\+=|-=|\*=|&=|\|=|\^=|%=|<<=|>>=|>>>=), :operator],
      [/===|!==|==|<=|>=|!=|&&|\|\||\+\+|--/, :operator],
      [/<<|>>|>>>/, :operator],
      [%r(=|>|<|!|~|\?|:|\+|-|\*|&|\||\^|%), :operator],
      [%r(/=|/), :operator22, {:only_after => [:numeric, :ident, :string, :regex]}],

      # string  改行は含められない
      [/"([^"]|\\.)*"/, :string],  #"
      [/'([^']|\\.)*'/, :string],  #'

      # 正規表現リテラル  / or /= と誤認の可能性
      # /* or // はコメントとしてよい。空の正規表現は/(?:)/と書く。
      [%r(/([^/]|\\.)*/[A-Za-z]?), :regex],

      [/\r?\n|\r/, :eol],
      [/\s+/, :space],
    ]

    KEYWORDS = 'break    else       new    var
                case     finally    return void
                catch    for        switch while
                continue function   this   with
                default  if         throw
                delete   in         try
                do       instanceof typeof
                null true false'.split(/\s+/)

    # Javaのキーワード？
    RESERVED = 'abstract enum       int       short
                boolean  export     interface static
                byte     extends    long      super
                char     final      native    synchronized
                class    float      package   throws
                const    goto       private   transient
                debugger implements protected volatile
                double   import     public'.split(/\s+/)

    def parse(source_, &block)
      @scanner = StringScanner.new(source_)
      @parsed = []
      @last_kind = :begin_of_source
      @cur.text = ''
      while !@scanner.eos?
        matched = false
        JS_PATTERNS.each {|pat, sym, opt|
          if opt
            next if opt[:only_after] && !opt[:only_after].include?(@last_kind)
          end
          r = @scanner.scan(pat)
          next if !r

          #puts "text = #{r}, sym = #{sym}, pat = #{pat}"
          if sym == :ident
            if KEYWORDS.include?(r)
              flush_token Token.new(:keyword, r)
            elsif RESERVED.include?(r)
              flush_token Token.new(:reserved, r)
            else
              flush_token Token.new(sym, r)
            end
          else
            flush_token Token.new(sym, r)
          end
          matched = true
          break
        }
        if !matched
          @last_kind = @cur.symbol = :other
          @cur.text << @scanner.getch
        end
      end
      return @parsed
    end

    # override
    def add_token token
      super
      if token.symbol != :space && token.symbol != :comment
        @last_kind = token.symbol
      end
    end
  end # of JavaLexer class
end

