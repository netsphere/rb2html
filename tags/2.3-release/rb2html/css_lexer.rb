
# CSS 2.1

require 'rb2html/pattern_lexer'

module Rb2HTML
  class CSSLexer < PatternLexer
    
    PAT = [
    [/\/\*.*?\*\//m, :comment],
    [/url\(/, :url_start],
    [/-?[_a-z][_a-z0-9-]*/, :ident],
    [/(['"]).*?\1/, :string],   #'
    ]
    
    PROPS = 'background-attachment
background-color
background-image
background-position
background-repeat
background
border-collapse
border-color
border-spacing
border-style
border-top
border-right
border-bottom
border-left
border-top-color
border-right-color
border-bottom-color
border-left-color
border-top-style
border-right-style
border-bottom-style
border-left-style
border-top-width
border-right-width
border-bottom-width
border-left-width
border-width
border
bottom
caption-side
clear
clip
color
content
counter-increment
counter-reset
cursor
direction
display
empty-cells
float
font-family
font-size
font-style
font-variant
font-weight
font
height
left
letter-spacing
line-height
list-style-image
list-style-position
list-style-type
list-style
margin-right
margin-left
margin-top
margin-bottom
margin
max-height
max-width
min-height
min-width
orphans
outline-color
outline-style
outline-width
outline
overflow
padding-top
padding-right
padding-bottom
padding-left
padding
page-break-after
page-break-before
page-break-inside
position
quotes
right
table-layout
text-align
text-decoration
text-indent
text-transform
top
unicode-bidi
vertical-align
visibility
white-space
widows
width
word-spacing
z-index'.split(/\s+/)

    def parse source, &block
      @s = StringScanner.new source
      @parsed = []
      @cur.text = ''
      while !@s.eos?
        matched = false
        PAT.each {|pat, sym|
          r = @s.scan pat
          if r
            if sym == :ident
              if PROPS.include? r
                flush_token Token.new(:property, r)
              else
                flush_token Token.new(sym, r)
              end
            else
              flush_token Token.new(sym, r)
            end
            matched = true
            break
          end
        }
        if !matched
          @cur.symbol = :other
          @cur.text << @s.getch
        end
      end
      flush_token
      return @parsed
    end
  end
end

if __FILE__ == $0
  h = Rb2HTML::CSSLexer.new
  pa = h.parse <<EOF
body {
  background:red url("pendant.png");
  background-repeat:repeat-y;
  background-attachment:fixed;
}
EOF
  pa.each {|t|
    puts "#{t.symbol}, '#{t.text}'"
  }
end
