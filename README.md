
Format the source code of the programming languages into cute HTML.

  - Ruby
  - Java
  - JavaScript
  - C++ / Objective-C
  - Python
  - Haskell
  - HTML/XML
  - CSS

See https://www.nslabs.jp/rb2html.rhtml
